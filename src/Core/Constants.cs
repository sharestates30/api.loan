﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core
{
    public static class Constants
    {
        public static class ParameterGroup
        {
            public static string BorrowerCreditScore = "BorrowerCreditScore";
            public static string HearAboutUs = "HearAboutUs";
            public static string RefinanceHolderDuration = "RefinanceHolderDuration";
            public static string RefinanceMortgagesDefaultReason = "RefinanceMortgagesDefaultReason";
            public static string RefinancePropertyWorkRange = "RefinancePropertyWorkRange";
            public static string YearsInRealestate = "YearsInRealestate";
            public static string BorrowerTransactionTotalAmount = "BorrowerTransactionTotalAmount";

            public static string OpenerStatusProject = "OpenerStatusProject";
            public static string OriginatorStatusProject = "OriginatorStatusProject";
            public static string ProcessingDepartmentStatusProject = "ProcessingDepartmentStatusProject";
            public static string UnderwritingStatusProject = "UnderwritingStatusProject";
            public static string ClosingDeskStatusProject = "ClosingDeskStatusProject";
            public static string PostClosingStatusProject = "PostClosingStatusProject";
            public static string TermSheetExpire = "TermSheetExpire";

        }

        public static class ActionTypeName {
            public static string New = "New";
            public static string Update = "Update";
            public static string ChangeStatus = "ChangeStatus";
            public static string Notification = "Notification";
            public static string Comment = "Comment";
            public static string Remove = "Remove";
        }

        public static class QueueNames
        {
            public const string LoanPendingQueue = "loans-loanpendingQueue";
            public const string LoanPendingToAuthorizeQueue = "loans-loanpendingtoauthorizeQueue";
            public const string LoanAcceptedQueue = "loans-loanacceptedQueue";
            public const string LoanUnderReviewTermSheetQueue = "loans-loanunderreviewtermsheetQueue";
            public const string LoanSignedTermSheetQueue = "loans-loansignedtermsheetQueue";
            public const string LoanExpiredTermSheetQueue = "loans-loanexpiredtermsheetQueue";
            public const string LoanAcceptedWithDepositQueue = "loans-loanacceptedwithdepositQueue";
            public const string LoanAppraisalOrderedQueue = "loans-loanapprasailorderedQueue";
            public const string LoanAppraisalReceivedQueue = "loans-loanapprasailreceivedQueue";
            public const string LoanAppraisalReportSentQueue = "loans-loanapprasailreportsentQueue";
            public const string LoanAppraisalApprovedQueue = "loans-loanapprasailapprovedQueue";
            public const string LoanTitleRequiredQueue = "loans-loantitlerequiredQueue";
            public const string LoanUWClearQueue = "loans-loanuwclearQueue";
            public const string LoanUWTitleClearQueue = "loans-loanuwtitleclearQueue";
            public const string LoanTitleReceivedQueue = "loans-loantitlereceivedQueue";
            public const string LoanLenderInstructionsIssuedQueue = "loans-loanlenderinstructionsissuedQueue";
            public const string LoanClosedQueue = "loans-loanclosedqueue";


            public const string LoanAssignUserQueue = "loans-assignUserQueue";
            public const string LoanRejectQueue = "loans-loanrejectQueue";
            public const string LoanCommentQueue = "loans-loancommentQueue";
            public const string LoanNotificationQueue = "loans-loannotificationQueue";
            public const string LoanCreateNotificationQueue = "loans-loancreatenotificationQueue";
            public const string LoanDocumentChangeStatusQueue = "loans-loandocumentchangestatusQueue";
        }

        public static class LoanStatus {
            public const string Draft = "1";
            public const string PendingToReview = "2";
            public const string PendingToAuthorize = "98";
            public const string Accepted = "3";
            public const string PendingAccepted = "1000";
            public const string TermSheetUnderReview = "4";
            public const string TermSheetReviewed = "90";
            public const string PendingTermSheetReviewed = "1001";
            public const string PendingTermsheetSignature = "5";
            public const string SignedTermSheet = "6";
            public const string ExpiredTermSheet = "7";
            public const string AcceptedWithDeposit = "8";
            public const string PendingAcceptedWithDeposit = "1002";
            public const string AppraisalReceived = "9";
            public const string PendingAppraisalReceived = "1003";
            public const string AppraisalOrdered = "10";
            public const string PendingAppraisalOrdered = "1004";
            public const string AppraisalReportSent = "11";
            public const string PendingAppraisalReportSent = "1005";
            public const string AppraisalApproved = "12";
            public const string PendingAppraisalApproved = "1006";
            public const string TitleRequired = "13";
            public const string UWClear = "14";
            public const string TitleReceived = "15";
            public const string UWTitleClear = "16";
            public const string LenderInstructionsIssued = "17";
            public const string Closed = "18";
            public const string PendingClosed = "1007";
            public const string Rejected = "99";
            public const string PendingRejected = "1008";
        }

        public static class RolesNames {
            public const string PostClosing = "post closing";
            public const string ComplianceOfficer = "compliance officer";
            public const string SubLoanServicer = "sub loan servicer";
            public const string SubUnderwriter = "sub underwriter";
            public const string SubClosingDesk = "sub closing desk";
            public const string SubProcessor = "sub processor";
            public const string SubPostCloser = "sub post closer";
            public const string SubOpener = "sub opener";
            public const string Processor = "processor";
            public const string LeadPostCloser = "lead post closer";
            public const string Opener = "opener";
            public const string LeadUnderwriter = "lead underwriter";
            public const string LeadOpener = "lead opener";
            public const string HeadLoanServicer = "head loan servicer";
            public const string LeadClosingDesk = "lead closing desk";
            public const string LeadProcessor = "lead processor";
            public const string ProjectUploader = "project uploader";
            public const string Accounting = "accounting";
            public const string Underwriter = "underwriter";
            public const string Admin = "admin";
            public const string LeadOriginator = "lead originator";
            public const string SubOriginator = "sub originator";
        }

        public static class StageGroupCode {
            public const string Loan = "LoanStage";
            public const string Pipeline = "PipelineStage";
        }

        public static class StageCode
        {
            public const string Draft = "Draft";
            public const string Pending = "Pending";
            public const string Approved = "Approved";
            public const string Pipeline = "Pipeline";
        }

        public static class LoanDocumentNames {
            public const string LoanApplicationForm = "Loan Application Form";
            public const string TermSheetForm = "Term Sheet";
            public const string TermSheetSignedForm = "Term Sheet Signed";
            public const string CreditAuthorizationForm = "Credit Authorization Form";
            public const string CreditCardAuthorizationForm = "Credit Card Authorization Form";
            public const string CreditCardDepositReceipt = "Credit Card Deposit Receipt";
            public const string Appraisal = "Appraisal";
            public const string AppraisalInvoice = "Appraisal Invoice";
            public const string ACHForm = "ACH Form";
            public const string BackgroundCheck = "Background Check";
            public const string SponsorTrackRecord = "Sponsor Track Record";
            public const string TitleApplicationForm = "Title Application Form";
            public const string TitleReport = "Title Report";
            public const string TitlePolicy = "Title Policy";
            public const string CertificateOfGoodStanding = "Certificate of Good Standing";
            public const string ArticlesOfOrganization = "Articles of Organization";
            public const string FilingReceipt = "Filing Receipt";
            public const string TINPrintOut = "TIN Print Out";
            public const string OperatingAgreement = "Operating Agreement";
            public const string ProofOfFunds = "Proof Of Funds";
            public const string FundingSourcesUses = "Funding Sources & Uses";
            public const string DisclosureSheet = "Disclosure Sheet";
            public const string Miscellaneous = "Miscellaneous";
            public const string LenderLetter = "Lender Letter";
            public const string RecordedMortgage = "Recorded Mortgage";
            public const string RecordedUCCs = "Recorded UCCs";
            public const string FinalLoanPolicy = "Final Loan Policy";
            public const string ClosingPackage = "Closing Package";
            public const string ForbearanceAgreement = "Forbearance Agreement";
            public const string PropertyTaxes = "Property Taxes";
        }

        public static class LoanDocumentCodes
        {
            public const string LoanApplicationForm = "LoanApplicationForm";
            public const string TermSheetForm = "TermSheetForm";
            public const string TermSheetSignedForm = "TermSheetSignedForm";
            public const string CreditAuthorizationForm = "CreditAuthorizationForm";
            public const string CreditCardAuthorizationForm = "CreditCardAuthorizationForm";
            public const string CreditCardDepositReceipt = "CreditCardDepositReceipt";
            public const string Appraisal = "Appraisal";
            public const string AppraisalInvoice = "AppraisalInvoice";
            public const string ACHForm = "ACHForm";
            public const string BackgroundCheck = "BackgroundCheck";
            public const string SponsorTrackRecord = "SponsorTrackRecord";
            public const string TitleApplicationForm = "TitleApplicationForm";
            public const string TitleReport = "TitleReport";
            public const string TitlePolicy = "TitlePolicy";
            public const string CertificateOfGoodStanding = "CertificateOfGoodStanding";
            public const string ArticlesOfOrganization = "ArticlesOfOrganization";
            public const string FilingReceipt = "FilingReceipt";
            public const string TINPrintOut = "TINPrintOut";
            public const string OperatingAgreement = "OperatingAgreement";
            public const string ProofOfFunds = "ProofOfFunds";
            public const string FundingSourcesUses = "FundingSourcesUses";
            public const string DisclosureSheet = "DisclosureSheet";
            public const string Miscellaneous = "Miscellaneous";
            public const string LenderLetter = "LenderLetter";
            public const string RecordedMortgage = "RecordedMortgage";
            public const string RecordedUCCs = "RecordedUCCs";
            public const string FinalLoanPolicy = "FinalLoanPolicy";
            public const string ClosingPackage = "ClosingPackage";
            public const string ForbearanceAgreement = "ForbearanceAgreement";
            public const string PropertyTaxes = "PropertyTaxes";
        }

        public static class StageNames
        {
            public const string DraftLoans = "Draft Loans";
            public const string PendingLoans = "Pending Loans";
            public const string ApprovedLoans = "Approved Loans";
            public const string TermSheetStage = "Term Sheet Stage";
            public const string AcceptDepositStageStage = "Accept w/ Deposit Stage";
            public const string AppraisalStage = "Appraisal Stage";
            public const string TitleCompanyStage = "Title Company Stage";
            public const string LenderInstructionsIssuedStage = "Lender Instructions Issued Stage";
            public const string CloseStage = "Close Stage";
        }



    }
}
