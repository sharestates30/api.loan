﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    /// <summary>
    /// Stage Group
    /// </summary>
    public class StageGroup : BaseEntity
    {
        public int StageGroupId { get; set; }
        public string StageGroupCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Stage> Stages { get; set; }
    }
}
