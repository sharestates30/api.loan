﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class StageStatus
    {
        public int StageStatusId { get; set; }
        public string ProcessStatus { get; set; }

        public int StageId { get; set; }
        public virtual Stage Stage { get; set; }
    }
}
