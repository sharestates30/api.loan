﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class Rol : BaseEntity
    {
        public int RolId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<UserRol> Users { get; set; }
    }
}
