﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace Entity.Configuration
{
    public static class DbInitializer
    {
        public static void Initialize(LoanDbContext context) {
            context.Database.EnsureCreated();

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.BorrowerCreditScore)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerCreditScore, Name = "Below 650", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerCreditScore, Name = "650-679", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerCreditScore, Name = "680-699", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerCreditScore, Name = "700-759", Value = "4", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerCreditScore, Name = "760-850", Value = "5", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.HearAboutUs)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Advertisement", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Borrower Referral", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Scotsman Guide", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Connected Investors", Value = "4", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Search Engine", Value = "5", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Social Media", Value = "6", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.HearAboutUs, Name = "Other", Value = "7", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.RefinanceHolderDuration)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceHolderDuration, Name = "1-3 months", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceHolderDuration, Name = "4-6 months", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceHolderDuration, Name = "6-12 months", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceHolderDuration, Name = "1-2 years", Value = "4", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceHolderDuration, Name = "3+ years", Value = "5", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.RefinanceMortgagesDefaultReason)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceMortgagesDefaultReason, Name = "Low Occupancy", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceMortgagesDefaultReason, Name = "Going through divorce", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceMortgagesDefaultReason, Name = "Ran out of money for the construction", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinanceMortgagesDefaultReason, Name = "Other", Value = "4", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.RefinancePropertyWorkRange)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinancePropertyWorkRange, Name = "Minimal", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinancePropertyWorkRange, Name = "Average", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.RefinancePropertyWorkRange, Name = "Extensive", Value = "3", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.YearsInRealestate)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.YearsInRealestate, Name = "Less than 1 year", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.YearsInRealestate, Name = "1-3 years", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.YearsInRealestate, Name = "3-5 years", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.YearsInRealestate, Name = "5-10 years", Value = "4", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.YearsInRealestate, Name = "10+ years", Value = "5", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.BorrowerTransactionTotalAmount)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerTransactionTotalAmount, Name = "< $500K", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerTransactionTotalAmount, Name = "$500K- 2MM", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerTransactionTotalAmount, Name = "$2 - $10MM", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerTransactionTotalAmount, Name = "$10 - 40MM", Value = "4", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.BorrowerTransactionTotalAmount, Name = "$40 - 100MM", Value = "5", IsActive = true });

                context.SaveChanges();
            }


            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.OpenerStatusProject)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Draft", Value = "1", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending Review", Value = "2", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Accepted", Value = "3", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Term Sheet Under Review", Value = "4", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Term Sheet Reviewed", Value = "90", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending Termsheet Signature", Value = "5", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Signed Term Sheet", Value = "6", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Expired Term Sheet", Value = "7", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending To Authorize", Value = "98", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Rejected", Value = "99", IsActive = true });

                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending Accepted", Value = "1000", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending TermSheet Reviewed", Value = "1001", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending Rejected", Value = "1008", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.OriginatorStatusProject)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OriginatorStatusProject, Name = "Accepted w/ Deposit", Value = "8", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.OpenerStatusProject, Name = "Pending Accepted w/ Deposit", Value = "1002", IsActive = true });
                context.SaveChanges();
            }


            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.UnderwritingStatusProject)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Appraisal Received", Value = "9", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Appraisal Ordered", Value = "10", IsActive = true });
                
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Appraisal Report Sent", Value = "11", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Appraisal Approved", Value = "12", IsActive = true });

                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Pending Appraisal Received", Value = "1003", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Pending Appraisal Ordered", Value = "1004", IsActive = true });
                
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Pending Appraisal Report Sent", Value = "1005", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Pending Appraisal Approved", Value = "1006", IsActive = true });


                context.SaveChanges();
            }


            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.ClosingDeskStatusProject)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.ClosingDeskStatusProject, Name = "Title Required", Value = "13", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.ClosingDeskStatusProject, Name = "UW Clear", Value = "14", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.ClosingDeskStatusProject, Name = "Title Received", Value = "15", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.ClosingDeskStatusProject, Name = "UW Title Clear", Value = "16", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.ClosingDeskStatusProject, Name = "Lender Instructions Issued", Value = "17", IsActive = true });

                
                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.PostClosingStatusProject)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.PostClosingStatusProject, Name = "Closed", Value = "18", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.PostClosingStatusProject, Name = "Paid Off", Value = "19", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.PostClosingStatusProject, Name = "Denied", Value = "20", IsActive = true });
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.PostClosingStatusProject, Name = "Dead", Value = "21", IsActive = true });

                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.PostClosingStatusProject, Name = "Pending Closed", Value = "1007", IsActive = true });

                context.SaveChanges();
            }

            if (!context.Parameters.Any(c => c.Code.Equals(Constants.ParameterGroup.TermSheetExpire)))
            {
                context.Parameters.Add(new Parameter { Code = Constants.ParameterGroup.TermSheetExpire, Name = "ExpireTermSheetDays", Value = "30", IsActive = true });

                context.SaveChanges();
            }

            if (!context.StageGroups.Any(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Loan)))
            {
                context.StageGroups.Add(new StageGroup { StageGroupCode = Constants.StageGroupCode.Loan, Name = "Loan Stage", Description = "Loan Stage" });

                context.SaveChanges();
            }

            if (!context.StageGroups.Any(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline)))
            {
                context.StageGroups.Add(new StageGroup { StageGroupCode = Constants.StageGroupCode.Pipeline, Name = "Pipeline Stage", Description = "Pipeline Stage" });

                context.SaveChanges();
            }


            var documents = new Dictionary<string, string>();

            documents.Add(Constants.LoanDocumentCodes.LoanApplicationForm, Constants.LoanDocumentNames.LoanApplicationForm);
            documents.Add(Constants.LoanDocumentCodes.CreditAuthorizationForm, Constants.LoanDocumentNames.CreditAuthorizationForm);
            documents.Add(Constants.LoanDocumentCodes.CreditCardAuthorizationForm, Constants.LoanDocumentNames.CreditCardAuthorizationForm);
            documents.Add(Constants.LoanDocumentCodes.CreditCardDepositReceipt, Constants.LoanDocumentNames.CreditCardDepositReceipt);
            documents.Add(Constants.LoanDocumentCodes.TermSheetForm, Constants.LoanDocumentNames.TermSheetForm);
            documents.Add(Constants.LoanDocumentCodes.TermSheetSignedForm, Constants.LoanDocumentNames.TermSheetSignedForm);
            documents.Add(Constants.LoanDocumentCodes.Appraisal, Constants.LoanDocumentNames.Appraisal);
            documents.Add(Constants.LoanDocumentCodes.AppraisalInvoice, Constants.LoanDocumentNames.AppraisalInvoice);
            documents.Add(Constants.LoanDocumentCodes.ACHForm, Constants.LoanDocumentNames.ACHForm);
            documents.Add(Constants.LoanDocumentCodes.BackgroundCheck, Constants.LoanDocumentNames.BackgroundCheck);
            documents.Add(Constants.LoanDocumentCodes.SponsorTrackRecord, Constants.LoanDocumentNames.SponsorTrackRecord);
            documents.Add(Constants.LoanDocumentCodes.TitleApplicationForm, Constants.LoanDocumentNames.TitleApplicationForm);
            documents.Add(Constants.LoanDocumentCodes.TitleReport, Constants.LoanDocumentNames.TitleReport);
            documents.Add(Constants.LoanDocumentCodes.TitlePolicy, Constants.LoanDocumentNames.TitlePolicy);
            documents.Add(Constants.LoanDocumentCodes.CertificateOfGoodStanding, Constants.LoanDocumentNames.CertificateOfGoodStanding);
            documents.Add(Constants.LoanDocumentCodes.ArticlesOfOrganization, Constants.LoanDocumentNames.ArticlesOfOrganization);
            documents.Add(Constants.LoanDocumentCodes.FilingReceipt, Constants.LoanDocumentNames.FilingReceipt);
            documents.Add(Constants.LoanDocumentCodes.TINPrintOut, Constants.LoanDocumentNames.TINPrintOut);
            documents.Add(Constants.LoanDocumentCodes.OperatingAgreement, Constants.LoanDocumentNames.OperatingAgreement);
            documents.Add(Constants.LoanDocumentCodes.ProofOfFunds, Constants.LoanDocumentNames.ProofOfFunds);
            documents.Add(Constants.LoanDocumentCodes.FundingSourcesUses, Constants.LoanDocumentNames.FundingSourcesUses);
            documents.Add(Constants.LoanDocumentCodes.DisclosureSheet, Constants.LoanDocumentNames.DisclosureSheet);
            documents.Add(Constants.LoanDocumentCodes.Miscellaneous, Constants.LoanDocumentNames.Miscellaneous);
            documents.Add(Constants.LoanDocumentCodes.LenderLetter, Constants.LoanDocumentNames.LenderLetter);
            documents.Add(Constants.LoanDocumentCodes.RecordedMortgage, Constants.LoanDocumentNames.RecordedMortgage);
            documents.Add(Constants.LoanDocumentCodes.RecordedUCCs, Constants.LoanDocumentNames.RecordedUCCs);
            documents.Add(Constants.LoanDocumentCodes.FinalLoanPolicy, Constants.LoanDocumentNames.FinalLoanPolicy);
            documents.Add(Constants.LoanDocumentCodes.ClosingPackage, Constants.LoanDocumentNames.ClosingPackage);
            documents.Add(Constants.LoanDocumentCodes.ForbearanceAgreement, Constants.LoanDocumentNames.ForbearanceAgreement);
            documents.Add(Constants.LoanDocumentCodes.PropertyTaxes, Constants.LoanDocumentNames.PropertyTaxes);

            var anyDocuments = false;
            var cont = 1;
            foreach (var item in documents)
            {
                if (!context.Documents.Any(c => c.DocumentCode.Equals(item.Key, StringComparison.OrdinalIgnoreCase)))
                {

                    DocumentUploadType internalDocument = DocumentUploadType.Internal;

                    if (item.Key.Equals(Constants.LoanDocumentCodes.CertificateOfGoodStanding) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.ArticlesOfOrganization) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.FilingReceipt) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.TINPrintOut) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.OperatingAgreement) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.ProofOfFunds) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.FundingSourcesUses) ||
                        item.Key.Equals(Constants.LoanDocumentCodes.DisclosureSheet))
                        internalDocument = DocumentUploadType.User;

                    context.Documents.Add(new Document
                    {
                        Name = item.Value,
                        Description = item.Value,
                        CreatedOn = DateTime.UtcNow,
                        DocumentCode = item.Key,
                        Order = cont,
                        NeedValidation = true,
                        InternalDocument = internalDocument
                    });
                    cont++;
                    anyDocuments = true;
                }
            }
            if (anyDocuments)
                context.SaveChanges();


            // Stage - Loans
            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.DraftLoans)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Loan));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Draft });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubOriginator });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadOriginator });

                context.Stages.Add(new Stage { StageGroupId = stageGroup.StageGroupId, StageCode = Constants.StageCode.Draft, Name = Constants.StageNames.DraftLoans, Description = Constants.StageNames.DraftLoans, Status = processStatus, Roles = roles });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.PendingLoans)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Loan));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingToReview });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingToAuthorize });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubOriginator });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadOriginator });

                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.LoanApplicationForm
                }.Contains(c.DocumentCode)).Select(c=> new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pending,
                    Name = Constants.StageNames.PendingLoans,
                    Description = Constants.StageNames.PendingLoans,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.ApprovedLoans)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Loan));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });

                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.LoanApplicationForm
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Approved,
                    Name = Constants.StageNames.ApprovedLoans,
                    Description = Constants.StageNames.ApprovedLoans,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            // Stage - Pipeline
            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.TermSheetStage)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.TermSheetUnderReview });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.TermSheetReviewed });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingTermSheetReviewed });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingTermsheetSignature });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.SignedTermSheet });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.ExpiredTermSheet });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadOpener });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubOpener });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadOriginator });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubOriginator });

                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.TermSheetForm,
                    Constants.LoanDocumentCodes.TermSheetSignedForm
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pipeline,
                    Name = Constants.StageNames.TermSheetStage,
                    Description = Constants.StageNames.TermSheetStage,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.AcceptDepositStageStage)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.AcceptedWithDeposit });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingAcceptedWithDeposit });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadUnderwriter });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubUnderwriter });

                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.CreditAuthorizationForm,
                    Constants.LoanDocumentCodes.CreditCardAuthorizationForm,
                    Constants.LoanDocumentCodes.CreditCardDepositReceipt
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pipeline,
                    Name = Constants.StageNames.AcceptDepositStageStage,
                    Description = Constants.StageNames.AcceptDepositStageStage,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.AppraisalStage)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.AppraisalOrdered });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingAppraisalOrdered });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.AppraisalReceived });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingAppraisalReceived });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.AppraisalReportSent });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingAppraisalReportSent });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.AppraisalApproved });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingAppraisalApproved });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadProcessor });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubProcessor });

                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.Appraisal,
                    Constants.LoanDocumentCodes.AppraisalInvoice
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pipeline,
                    Name = Constants.StageNames.AppraisalStage,
                    Description = Constants.StageNames.AppraisalStage,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.TitleCompanyStage)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.TitleRequired });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.UWClear });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.TitleReceived });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.UWTitleClear });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.LeadUnderwriter });
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.SubUnderwriter });


                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.TitleApplicationForm,
                    Constants.LoanDocumentCodes.TitlePolicy,
                    Constants.LoanDocumentCodes.TitleReport
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pipeline,
                    Name = Constants.StageNames.TitleCompanyStage,
                    Description = Constants.StageNames.TitleCompanyStage,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.LenderInstructionsIssuedStage)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.LenderInstructionsIssued });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });

                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.LenderLetter
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pipeline,
                    Name = Constants.StageNames.LenderInstructionsIssuedStage,
                    Description = Constants.StageNames.LenderInstructionsIssuedStage,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }

            if (!context.Stages.Any(c => c.Name.Equals(Constants.StageNames.CloseStage)))
            {
                var stageGroup = context.StageGroups.FirstOrDefault(c => c.StageGroupCode.Equals(Constants.StageGroupCode.Pipeline));

                var processStatus = new List<StageStatus>();
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Accepted });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.Closed });
                processStatus.Add(new StageStatus { ProcessStatus = Constants.LoanStatus.PendingClosed });

                var roles = new List<StageRole>();
                //roles.Add(new StageRole { RoleId = Constants.RolesNames.Admin });


                var docs = context.Documents.Where(c => new List<string>() {
                    Constants.LoanDocumentCodes.RecordedMortgage,
                    Constants.LoanDocumentCodes.RecordedUCCs,
                    Constants.LoanDocumentCodes.FinalLoanPolicy,
                    Constants.LoanDocumentCodes.ClosingPackage,
                    Constants.LoanDocumentCodes.ForbearanceAgreement,
                    Constants.LoanDocumentCodes.PropertyTaxes
                }.Contains(c.DocumentCode)).Select(c => new DocumentStage { DocumentId = c.DocumentId, CreatedOn = DateTime.UtcNow }).ToList();

                context.Stages.Add(new Stage
                {
                    StageGroupId = stageGroup.StageGroupId,
                    StageCode = Constants.StageCode.Pipeline,
                    Name = Constants.StageNames.CloseStage,
                    Description = Constants.StageNames.CloseStage,
                    Status = processStatus,
                    Roles = roles,
                    Documents = docs
                });

                context.SaveChanges();
            }


        }
    }
}
