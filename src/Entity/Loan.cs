﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity
{
    public class Loan : BaseEntity
    {

        /// <summary>
        /// Loan Application Information
        /// </summary>
        public Guid LoanId { get; set; }
        public string LoanNumber { get; set; }
        public bool IsPipeline { get; set; }
        public bool IsApproved { get; set; }

        #region " < ================== Transaction Detail ================== > "

        public LoanPurposeTypeEnum LoanPurposeType { get; set; }
        public TransactionTypeEnum TransactionType { get; set; }
        public bool HasSalesRepresentative { get; set; }
        public string SalesPersonId { get; set; }
        public string SalesPersonName { get; set; }

        public bool HasRehabComponent { get; set; }

        public decimal PurchasePrice { get; set; }
        public decimal ConstructionBudget { get; set; }

        public decimal AsIsValue { get; set; }
        public decimal FinalValue { get; set; }
        public decimal AquisitionLoanRequest { get; set; }
        public decimal ConstructionLoanRequest { get; set; }

        public DateTime? OriginalPurchaseDate { get; set; }
        public decimal? OriginalPurchasePrice { get; set; }

        /// <summary>
        /// 12 months
        /// 24 months
        /// 36 months
        /// </summary>
        public int LoanTermRequest { get; set; }
        public bool NeedMoreTerm { get; set; }
        public int? MoreTermValue { get; set; }

        public DateTime DesiredFundingDate { get; set; }
        public bool IsTOETransaction { get; set; }
        public DateTime? TOEClosingDate { get; set; }

        public bool IsShortSaleTransactionType { get; set; }
        public bool HasAssignmentFlipType { get; set; }
        public decimal? ContractorAssignedValue { get; set; }
        public string AssignorName { get; set; }
        public bool HasAssignorAffiliate { get; set; }
        public string AssignorAffiliateExplanation { get; set; }

        public bool IsLLCTransactionType { get; set; }

        public Guid? EntityId { get; set; }
        public string EntityName { get; set; }

        #endregion

        #region " < =================== Property Detail ==================== > "

        public string Address { get; set; }
        public string Address2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }

        public AssetTypeEnum CurrentAssetType { get; set; }
        public int CurrentAssetTypeValue { get; set; }

        public OccupancyTypeEnum Occupancy { get; set; }

        public AssetTypeEnum NewAssetType { get; set; }
        public int NewAssetTypeValue { get; set; }

        public DevelopmentPhaseTypeEnum DevelopmentPhase { get; set; }

        public ExitStrategyTypeEnum ExitStrategy { get; set; }

        #endregion

        #region " < ==================== Broker Detail ===================== > "

        public Guid? BrokerId { get; set; }
        public string BrokerAgentName { get; set; }
        public string BrokerEntityName { get; set; }

        #endregion

        #region " < ==================== Borrower Detail =================== > "

        public Guid? BorrowerId { get; set; }
        public string BorrowerName { get; set; }
        public string BorrowerEntityName { get; set; }

        /// <summary>
        /// Below 650
        /// 650-679
        /// 680-699
        /// 700-759
        /// 760-850
        /// </summary>
        public int BorrowerCreditScore { get; set; }

        public bool HasBankruptcyType { get; set; }
        public string BankruptcyExplanation { get; set; }
        public int BankruptcyYears { get; set; }

        public bool HasDefaultedLoanType { get; set; }
        public string DefaultedLoanExplanation { get; set; }

        public bool HasFiledForeclosure { get; set; }
        public string FiledForeclosureExplanation { get; set; }

        public bool HasOutstandingJudgmentsType { get; set; }
        public string OutstandingJudgmentsExplanation { get; set; }

        #endregion

        #region " < =================== Appraiser Detail =================== > "

        public Guid? AppraiserId { get; set; }
        public string AppraisalCompanyName { get; set; }
        public string AppraiserName { get; set; }
        public decimal? AppraiserFee { get; set; }
        public DateTime? AppraisalDateOrdered { get; set; }
        public DateTime? AppraisalDateNeeded { get; set; }

        /// <summary>
        /// Pending
        /// Received
        /// Ordered
        /// </summary>
        public string AppraisalStatus { get; set; }

        #endregion

        #region " < ===================== Title Company ==================== > "

        public Guid? TitleCompanyId { get; set; }
        public string TitlePropertyNumber { get; set; }
        public string TitleBusinessName { get; set; }
        public string TitleCompanyFirstName { get; set; }
        public string TitleCompanyLastName { get; set; }

        #endregion

        public string ProcessStatus { get; set; }
        public string TermSheetStatus { get; set; }
        
        public DateTime? SubmittedDay { get; set; }

        public string ReasonRejected { get; set; }

        public string OriginationEntity { get; set; }
        public DateTime? TargetClosingDate { get; set; }
        public DateTime? ClosingDate { get; set; }
        public string Location { get; set; }
        public int TimeOfClosing { get; set; }
		public DateTime? ClosingTime { get; set; }
		public decimal DisbursedAmountatClosing { get; set; }
        public decimal EscrowHoldBack { get; set; }
        
        public DateTime? TitleReportOrderedDate { get; set; }
        public DateTime? TitleReportBackDate { get; set; }
        public string TitleReportStatus { get; set; }

        public virtual Insurance Insurance { get; set; }
        public virtual TermSheet TermSheet { get; set; }

        [JsonIgnore]
        public virtual ICollection<LoanAuction> Auctions { get; set; }
        [JsonIgnore]
        public virtual ICollection<LoanLog> Logs { get; set; }
        [JsonIgnore]
        public virtual ICollection<LoanTracking> Trackings { get; set; }
        [JsonIgnore]
        public virtual ICollection<LoanDocument> Documents { get; set; }
        [JsonIgnore]
        public virtual ICollection<LoanTransaction> Transactions { get; set; }
        [JsonIgnore]
        public virtual ICollection<AdditionalProperty> AdditionalProperties { get; set; }

        public void AddLog(string action, string oldData, string newData, string createdBy, string createdByName) {
            if (this.Logs == null)
                this.Logs = new List<LoanLog>();

            this.Logs.Add(new LoanLog
            {
                Action = action,
                OldData = oldData,
                NewData = newData,
                CreatedBy = createdBy,
                CreatedByName = createdByName,
                CreatedOn = DateTime.UtcNow
            });
        }

        public void AddAdditionalProperties(string address, string state, string city, string zipCode, string country, string createdBy, string createdByName)
        {
            if (this.AdditionalProperties == null)
                this.AdditionalProperties = new List<AdditionalProperty>();

            this.AdditionalProperties.Add(new AdditionalProperty
            {
                Address = address,
                State = state,
                City = city,
                ZipCode = zipCode,
                Country = country,
                CreatedBy = createdBy,
                CreatedByName = createdByName,
                CreatedOn = DateTime.UtcNow
            });
        }

        public void Awarded(Guid auctionId, AuctionTypeEnum type) {
            if (this.Auctions == null)
                this.Auctions = new List<LoanAuction>();

            if (this.Auctions.Any(c => c.LoanAuctionId.Equals(auctionId))) {

                if (this.Auctions.Any(c => c.Awarded && c.LoanAuctionType.Equals(type)))
                    return;

                this.Auctions.FirstOrDefault(c => c.LoanAuctionId.Equals(auctionId)).Awarded = true;

                switch (type)
                {
                    //case AuctionTypeEnum.Appraiser:
                    //    this.AuctionClosedForAppraiser = true;
                    //    break;
                    //case AuctionTypeEnum.TileCompany:
                    //    this.AuctionClosedForTitleCompany = true;
                    //    break;
                    //case AuctionTypeEnum.Attorney:
                    //    this.AuctionClosedForAttorney = true;
                    //    break;
                    //default:
                    //    break;
                }

            }

        }

        public void AddBid(Guid vendorId, string vendorName, decimal bid, AuctionTypeEnum type, string content, string createdBy, string createdByName)
        {
            if (this.Auctions == null)
                this.Auctions = new List<LoanAuction>();

            this.Auctions.Add(new LoanAuction
            {
                VendorId = vendorId,
                VendorName = vendorName,
                Bid = bid,
                Awarded = false,
                LoanAuctionType = type,
                CreatedBy = createdBy, 
                CreatedByName = createdByName,
                CreatedOn = DateTime.UtcNow
            });
        }

        public void AddTransaction(string transactionId, string createdBy, string createdByName)
        {
            if (this.Transactions == null)
                this.Transactions = new List<LoanTransaction>();

            this.Transactions.Add(new LoanTransaction
            {
                TransactionId = transactionId,
                CreatedBy = createdBy,
                CreatedByName = createdByName,
                CreatedOn = DateTime.UtcNow
            });
        }
        
        public void TermSheetChangeStatus(string statusId, string status, string updatedBy, string updatedByName, string content)
        {
            this.AddTracking(status, updatedBy, updatedByName, content);

            this.TermSheetStatus = statusId;
            this.UpdatedBy = updatedBy;
            this.UpdatedByName = updatedByName;
            this.UpdatedOn = DateTime.UtcNow;
        }

        public void ChangeStatus(string statusId, string status, string updatedBy, string updatedByName, string content) {
            this.AddTracking(status, updatedBy, updatedByName, content);

            this.ProcessStatus = statusId;
            this.UpdatedBy = updatedBy;
            this.UpdatedByName = updatedByName;
            this.UpdatedOn = DateTime.UtcNow;
        }

        public void AddTracking(string status, string createdBy, string createdByName, string content) {
            if (this.Trackings == null)
                this.Trackings = new List<LoanTracking>();

            var code = string.Empty;
            if (this.Trackings.Any(c => c.Status.Equals(status)))
            {
                var count = this.Trackings.Where(c => c.Status.Equals(status)).Count();

                code = $"{status.ToString().Substring(0, 3)}-{count.ToString("000") + 1}";
            }
            else {
                code = $"{status.ToString().Substring(0, 3)}-{1.ToString("000")}";
            }

            this.Trackings.Add(new LoanTracking {
                Status = status,
                Code = code,
                Content = content,
                CreatedBy = createdBy,
                CreatedByName = createdByName,
                CreatedOn = DateTime.UtcNow
            });
        }
    }
}
