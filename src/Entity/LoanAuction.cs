﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class LoanAuction : BaseEntity
    {
        public Guid LoanAuctionId { get; set; }
        
        public Guid VendorId { get; set; }
        public AuctionTypeEnum LoanAuctionType { get; set; }
        public string VendorName { get; set; }

        public decimal Bid { get; set; }
        public string Content { get; set; }

        public bool Awarded { get; set; }

        public Guid LoanId { get; set; }
        public virtual Loan Loan { get; set; }
        
    }
}
