﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity
{
    public class LoanDbContext : DbContext
    {
        public LoanDbContext(DbContextOptions<LoanDbContext> options)
            : base(options)
        {

        }

        public DbSet<Loan> Loans { get; set; }
        public DbSet<LoanDocument> LoanDocuments { get; set; }
        public DbSet<LoanAuction> Auctions { get; set; }
        public DbSet<LoanDocumentFile> LoanDocumentFiles { get; set; }
        public DbSet<Insurance> Insurances { get; set; }
        public DbSet<TermSheet> TermSheets { get; set; }
        public DbSet<LoanTracking> LoanTrackings { get; set; }
        public DbSet<LoanTransaction> LoanTransactions { get; set; }
        public DbSet<Parameter> Parameters { get; set; }

        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentStage> DocumentStages { get; set; }

        public DbSet<Stage> Stages { get; set; }
        public DbSet<StageRole> StageRoles { get; set; }
        public DbSet<StageStatus> StageStatus { get; set; }
        public DbSet<StageGroup> StageGroups { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<UserRol> Users { get; set; }
    }
}
