﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class RenameColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ssignorAffiliateExplanation",
                table: "Loans",
                newName: "AssignorAffiliateExplanation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AssignorAffiliateExplanation",
                table: "Loans",
                newName: "ssignorAffiliateExplanation");
        }
    }
}
