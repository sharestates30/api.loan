﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Entity.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Documents",
                columns: table => new
                {
                    DocumentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowShared = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DocumentCode = table.Column<string>(nullable: true),
                    InternalDocument = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NeedValidation = table.Column<bool>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documents", x => x.DocumentId);
                });

            migrationBuilder.CreateTable(
                name: "Loans",
                columns: table => new
                {
                    LoanId = table.Column<Guid>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    AppraisalCompanyName = table.Column<string>(nullable: true),
                    AppraisalDateNeeded = table.Column<DateTime>(nullable: true),
                    AppraisalDateOrdered = table.Column<DateTime>(nullable: true),
                    AppraisalStatus = table.Column<string>(nullable: true),
                    AppraiserFee = table.Column<decimal>(nullable: true),
                    AppraiserId = table.Column<Guid>(nullable: true),
                    AppraiserName = table.Column<string>(nullable: true),
                    AquisitionLoanRequest = table.Column<decimal>(nullable: false),
                    AsIsValue = table.Column<decimal>(nullable: false),
                    AssignorName = table.Column<string>(nullable: true),
                    BankruptcyExplanation = table.Column<string>(nullable: true),
                    BankruptcyYears = table.Column<int>(nullable: false),
                    BorrowerCreditScore = table.Column<int>(nullable: false),
                    BorrowerEntityName = table.Column<string>(nullable: true),
                    BorrowerId = table.Column<Guid>(nullable: true),
                    BorrowerName = table.Column<string>(nullable: true),
                    BrokerAgentName = table.Column<string>(nullable: true),
                    BrokerEntityName = table.Column<string>(nullable: true),
                    BrokerId = table.Column<Guid>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    ClosingDate = table.Column<DateTime>(nullable: true),
                    ClosingTime = table.Column<DateTime>(nullable: true),
                    ConstructionBudget = table.Column<decimal>(nullable: false),
                    ConstructionLoanRequest = table.Column<decimal>(nullable: false),
                    ContractorAssignedValue = table.Column<decimal>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CurrentAssetType = table.Column<int>(nullable: false),
                    CurrentAssetTypeValue = table.Column<int>(nullable: false),
                    DefaultedLoanExplanation = table.Column<string>(nullable: true),
                    DesiredFundingDate = table.Column<DateTime>(nullable: false),
                    DevelopmentPhase = table.Column<int>(nullable: false),
                    DisbursedAmountatClosing = table.Column<decimal>(nullable: false),
                    EntityId = table.Column<Guid>(nullable: true),
                    EntityName = table.Column<string>(nullable: true),
                    EscrowHoldBack = table.Column<decimal>(nullable: false),
                    ExitStrategy = table.Column<int>(nullable: false),
                    FiledForeclosureExplanation = table.Column<string>(nullable: true),
                    FinalValue = table.Column<decimal>(nullable: false),
                    HasAssignmentFlipType = table.Column<bool>(nullable: false),
                    HasAssignorAffiliate = table.Column<bool>(nullable: false),
                    HasBankruptcyType = table.Column<bool>(nullable: false),
                    HasDefaultedLoanType = table.Column<bool>(nullable: false),
                    HasFiledForeclosure = table.Column<bool>(nullable: false),
                    HasOutstandingJudgmentsType = table.Column<bool>(nullable: false),
                    HasRehabComponent = table.Column<bool>(nullable: false),
                    HasSalesRepresentative = table.Column<bool>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    IsLLCTransactionType = table.Column<bool>(nullable: false),
                    IsPipeline = table.Column<bool>(nullable: false),
                    IsShortSaleTransactionType = table.Column<bool>(nullable: false),
                    IsTOETransaction = table.Column<bool>(nullable: false),
                    LoanNumber = table.Column<string>(nullable: true),
                    LoanPurposeType = table.Column<int>(nullable: false),
                    LoanTermRequest = table.Column<int>(nullable: false),
                    Location = table.Column<string>(nullable: true),
                    MoreTermValue = table.Column<int>(nullable: true),
                    NeedMoreTerm = table.Column<bool>(nullable: false),
                    NewAssetType = table.Column<int>(nullable: false),
                    NewAssetTypeValue = table.Column<int>(nullable: false),
                    Occupancy = table.Column<int>(nullable: false),
                    OriginalPurchaseDate = table.Column<DateTime>(nullable: true),
                    OriginalPurchasePrice = table.Column<decimal>(nullable: true),
                    OriginationEntity = table.Column<string>(nullable: true),
                    OutstandingJudgmentsExplanation = table.Column<string>(nullable: true),
                    ProcessStatus = table.Column<string>(nullable: true),
                    PurchasePrice = table.Column<decimal>(nullable: false),
                    ReasonRejected = table.Column<string>(nullable: true),
                    SalesPersonId = table.Column<string>(nullable: true),
                    SalesPersonName = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    SubmittedDay = table.Column<DateTime>(nullable: true),
                    TOEClosingDate = table.Column<DateTime>(nullable: true),
                    TargetClosingDate = table.Column<DateTime>(nullable: true),
                    TermSheetStatus = table.Column<string>(nullable: true),
                    TimeOfClosing = table.Column<int>(nullable: false),
                    TitleBusinessName = table.Column<string>(nullable: true),
                    TitleCompanyFirstName = table.Column<string>(nullable: true),
                    TitleCompanyId = table.Column<Guid>(nullable: true),
                    TitleCompanyLastName = table.Column<string>(nullable: true),
                    TitlePropertyNumber = table.Column<string>(nullable: true),
                    TitleReportBackDate = table.Column<DateTime>(nullable: true),
                    TitleReportOrderedDate = table.Column<DateTime>(nullable: true),
                    TitleReportStatus = table.Column<string>(nullable: true),
                    TransactionType = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    ssignorAffiliateExplanation = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Loans", x => x.LoanId);
                });

            migrationBuilder.CreateTable(
                name: "Parameters",
                columns: table => new
                {
                    ParameterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameters", x => x.ParameterId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RolId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RolId);
                });

            migrationBuilder.CreateTable(
                name: "StageGroups",
                columns: table => new
                {
                    StageGroupId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StageGroupCode = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StageGroups", x => x.StageGroupId);
                });

            migrationBuilder.CreateTable(
                name: "Insurances",
                columns: table => new
                {
                    LoanId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    FloodInsuranceAmount = table.Column<decimal>(nullable: true),
                    FloodInsuranceGoodThroughDate = table.Column<DateTime>(nullable: true),
                    FloodInsurancePaidReceiptFileId = table.Column<string>(nullable: true),
                    FloodInsurancePaidReceiptFileLink = table.Column<string>(nullable: true),
                    FloodInsurancePaidReceiptFileName = table.Column<string>(nullable: true),
                    FloodInsurancePolicyFileId = table.Column<string>(nullable: true),
                    FloodInsurancePolicyFileLink = table.Column<string>(nullable: true),
                    FloodInsurancePolicyFileName = table.Column<string>(nullable: true),
                    HazardInsuranceAmount = table.Column<decimal>(nullable: true),
                    HazardInsuranceGoodThroughDate = table.Column<DateTime>(nullable: true),
                    HazardInsurancePaidReceiptFileId = table.Column<string>(nullable: true),
                    HazardInsurancePaidReceiptFileLink = table.Column<string>(nullable: true),
                    HazardInsurancePaidReceiptFileName = table.Column<string>(nullable: true),
                    HazardInsurancePolicyFileId = table.Column<string>(nullable: true),
                    HazardInsurancePolicyFileLink = table.Column<string>(nullable: true),
                    HazardInsurancePolicyFileName = table.Column<string>(nullable: true),
                    IsFloodInsuranceRequired = table.Column<bool>(nullable: false),
                    IsHazardInsuranceRequired = table.Column<bool>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Insurances", x => x.LoanId);
                    table.ForeignKey(
                        name: "FK_Insurances_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Auctions",
                columns: table => new
                {
                    LoanAuctionId = table.Column<Guid>(nullable: false),
                    Awarded = table.Column<bool>(nullable: false),
                    Bid = table.Column<decimal>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LoanAuctionType = table.Column<int>(nullable: false),
                    LoanId = table.Column<Guid>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    VendorId = table.Column<Guid>(nullable: false),
                    VendorName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auctions", x => x.LoanAuctionId);
                    table.ForeignKey(
                        name: "FK_Auctions_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanDocuments",
                columns: table => new
                {
                    LoanDocumentId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DocumentId = table.Column<int>(nullable: false),
                    LoanId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanDocuments", x => x.LoanDocumentId);
                    table.ForeignKey(
                        name: "FK_LoanDocuments_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "DocumentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LoanDocuments_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanLog",
                columns: table => new
                {
                    LoanLogId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Action = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LoanId = table.Column<Guid>(nullable: false),
                    NewData = table.Column<string>(nullable: true),
                    OldData = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanLog", x => x.LoanLogId);
                    table.ForeignKey(
                        name: "FK_LoanLog_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanTrackings",
                columns: table => new
                {
                    LoanTrackingId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LoanId = table.Column<Guid>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanTrackings", x => x.LoanTrackingId);
                    table.ForeignKey(
                        name: "FK_LoanTrackings_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanTransactions",
                columns: table => new
                {
                    LoanTransactionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    LoanId = table.Column<Guid>(nullable: false),
                    LoanTransactionId1 = table.Column<int>(nullable: true),
                    Message = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionId = table.Column<string>(nullable: true),
                    TransactionStatus = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanTransactions", x => x.LoanTransactionId);
                    table.ForeignKey(
                        name: "FK_LoanTransactions_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LoanTransactions_LoanTransactions_LoanTransactionId1",
                        column: x => x.LoanTransactionId1,
                        principalTable: "LoanTransactions",
                        principalColumn: "LoanTransactionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TermSheets",
                columns: table => new
                {
                    LoanId = table.Column<Guid>(nullable: false),
                    AmountExistingLiens = table.Column<decimal>(nullable: false),
                    AsIsValueNeedfromSharestatesOrderedAppraiser = table.Column<decimal>(nullable: false),
                    Assumability = table.Column<decimal>(nullable: false),
                    BankAttorneyFee = table.Column<decimal>(nullable: false),
                    BorrowerForm = table.Column<string>(nullable: true),
                    BrokerFee = table.Column<decimal>(nullable: false),
                    ClosingCostsFinanced = table.Column<decimal>(nullable: false),
                    Collateral = table.Column<string>(nullable: true),
                    ConstructionDraws = table.Column<decimal>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CrossDefault = table.Column<string>(nullable: true),
                    DownPayment = table.Column<decimal>(nullable: false),
                    EarlyPayoff = table.Column<decimal>(nullable: false),
                    EstimatedAppraisal = table.Column<decimal>(nullable: false),
                    EstimatedTitleClosingCosts = table.Column<decimal>(nullable: false),
                    ExtensionOptions = table.Column<string>(nullable: true),
                    Guarantor = table.Column<string>(nullable: true),
                    HazardInsurance = table.Column<string>(nullable: true),
                    InterestAccrual = table.Column<decimal>(nullable: false),
                    InterestReserve = table.Column<decimal>(nullable: false),
                    InvestmentsOriginationPoints = table.Column<decimal>(nullable: false),
                    LTV = table.Column<decimal>(nullable: false),
                    Lender = table.Column<string>(nullable: true),
                    LoanAmountMayNotExceedARV = table.Column<decimal>(nullable: false),
                    LoanAmountMayNotExceedAsIsValue = table.Column<decimal>(nullable: false),
                    Payments = table.Column<decimal>(nullable: false),
                    PercentageClosingCostsFinanced = table.Column<decimal>(nullable: false),
                    PercentageRehabCostFinanced = table.Column<decimal>(nullable: false),
                    Points = table.Column<decimal>(nullable: false),
                    PowerAttorney = table.Column<string>(nullable: true),
                    PurposeRefinance = table.Column<decimal>(nullable: false),
                    Rate = table.Column<decimal>(nullable: false),
                    RefinanceClosingCostsFinanced = table.Column<decimal>(nullable: false),
                    RehabCost = table.Column<decimal>(nullable: false),
                    RehabCostFinanced = table.Column<decimal>(nullable: false),
                    SecondaryFinance = table.Column<string>(nullable: true),
                    SingleEntity = table.Column<string>(nullable: true),
                    TermSheetBinding = table.Column<decimal>(nullable: false),
                    TitleInsurance = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TermSheets", x => x.LoanId);
                    table.ForeignKey(
                        name: "FK_TermSheets_Loans_LoanId",
                        column: x => x.LoanId,
                        principalTable: "Loans",
                        principalColumn: "LoanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserRolId = table.Column<Guid>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    RolId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserRolId);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RolId",
                        column: x => x.RolId,
                        principalTable: "Roles",
                        principalColumn: "RolId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stages",
                columns: table => new
                {
                    StageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    StageCode = table.Column<string>(nullable: true),
                    StageGroupId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stages", x => x.StageId);
                    table.ForeignKey(
                        name: "FK_Stages_StageGroups_StageGroupId",
                        column: x => x.StageGroupId,
                        principalTable: "StageGroups",
                        principalColumn: "StageGroupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoanDocumentFiles",
                columns: table => new
                {
                    LoanDocumentFileId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    FileId = table.Column<string>(nullable: true),
                    FileLink = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    LoanDocumentId = table.Column<long>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanDocumentFiles", x => x.LoanDocumentFileId);
                    table.ForeignKey(
                        name: "FK_LoanDocumentFiles_LoanDocuments_LoanDocumentId",
                        column: x => x.LoanDocumentId,
                        principalTable: "LoanDocuments",
                        principalColumn: "LoanDocumentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DocumentStages",
                columns: table => new
                {
                    DocumentStageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DocumentId = table.Column<int>(nullable: false),
                    StageId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DocumentStages", x => x.DocumentStageId);
                    table.ForeignKey(
                        name: "FK_DocumentStages_Documents_DocumentId",
                        column: x => x.DocumentId,
                        principalTable: "Documents",
                        principalColumn: "DocumentId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DocumentStages_Stages_StageId",
                        column: x => x.StageId,
                        principalTable: "Stages",
                        principalColumn: "StageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StageRoles",
                columns: table => new
                {
                    StageRoleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedByName = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    RolId = table.Column<int>(nullable: true),
                    RoleId = table.Column<int>(nullable: false),
                    StageId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<string>(nullable: true),
                    UpdatedByName = table.Column<string>(nullable: true),
                    UpdatedOn = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StageRoles", x => x.StageRoleId);
                    table.ForeignKey(
                        name: "FK_StageRoles_Roles_RolId",
                        column: x => x.RolId,
                        principalTable: "Roles",
                        principalColumn: "RolId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StageRoles_Stages_StageId",
                        column: x => x.StageId,
                        principalTable: "Stages",
                        principalColumn: "StageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StageStatus",
                columns: table => new
                {
                    StageStatusId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProcessStatus = table.Column<string>(nullable: true),
                    StageId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StageStatus", x => x.StageStatusId);
                    table.ForeignKey(
                        name: "FK_StageStatus_Stages_StageId",
                        column: x => x.StageId,
                        principalTable: "Stages",
                        principalColumn: "StageId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStages_DocumentId",
                table: "DocumentStages",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentStages_StageId",
                table: "DocumentStages",
                column: "StageId");

            migrationBuilder.CreateIndex(
                name: "IX_Auctions_LoanId",
                table: "Auctions",
                column: "LoanId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanDocuments_DocumentId",
                table: "LoanDocuments",
                column: "DocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanDocuments_LoanId",
                table: "LoanDocuments",
                column: "LoanId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanDocumentFiles_LoanDocumentId",
                table: "LoanDocumentFiles",
                column: "LoanDocumentId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanLog_LoanId",
                table: "LoanLog",
                column: "LoanId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanTrackings_LoanId",
                table: "LoanTrackings",
                column: "LoanId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanTransactions_LoanId",
                table: "LoanTransactions",
                column: "LoanId");

            migrationBuilder.CreateIndex(
                name: "IX_LoanTransactions_LoanTransactionId1",
                table: "LoanTransactions",
                column: "LoanTransactionId1");

            migrationBuilder.CreateIndex(
                name: "IX_Stages_StageGroupId",
                table: "Stages",
                column: "StageGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_StageRoles_RolId",
                table: "StageRoles",
                column: "RolId");

            migrationBuilder.CreateIndex(
                name: "IX_StageRoles_StageId",
                table: "StageRoles",
                column: "StageId");

            migrationBuilder.CreateIndex(
                name: "IX_StageStatus_StageId",
                table: "StageStatus",
                column: "StageId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RolId",
                table: "Users",
                column: "RolId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DocumentStages");

            migrationBuilder.DropTable(
                name: "Insurances");

            migrationBuilder.DropTable(
                name: "Auctions");

            migrationBuilder.DropTable(
                name: "LoanDocumentFiles");

            migrationBuilder.DropTable(
                name: "LoanLog");

            migrationBuilder.DropTable(
                name: "LoanTrackings");

            migrationBuilder.DropTable(
                name: "LoanTransactions");

            migrationBuilder.DropTable(
                name: "Parameters");

            migrationBuilder.DropTable(
                name: "StageRoles");

            migrationBuilder.DropTable(
                name: "StageStatus");

            migrationBuilder.DropTable(
                name: "TermSheets");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "LoanDocuments");

            migrationBuilder.DropTable(
                name: "Stages");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Documents");

            migrationBuilder.DropTable(
                name: "Loans");

            migrationBuilder.DropTable(
                name: "StageGroups");
        }
    }
}
