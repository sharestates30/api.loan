﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class UserRol : BaseEntity
    {
        /// <summary>
        /// User Id reference from users
        /// </summary>
        public Guid UserRolId { get; set; }
        public string UserName { get; set; }

        public int RolId { get; set; }
        public virtual Rol Rol { get; set; }
    }
}
