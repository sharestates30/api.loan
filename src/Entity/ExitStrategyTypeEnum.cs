﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entity
{
    public enum ExitStrategyTypeEnum
    {
        Refinance,
        Sell,
        RefinanceOrSell
    }
}
