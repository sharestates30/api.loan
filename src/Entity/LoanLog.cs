﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity
{
    public class LoanLog : BaseEntity
    {
        public long LoanLogId { get; set; }
        public string Action { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }

        public Guid LoanId { get; set; }
        public virtual Loan Loan { get; set; }
    }
}
