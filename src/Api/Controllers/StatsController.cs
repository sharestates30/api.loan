﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    [Route("[controller]")]
    public class StatsController : BaseController
    {
        private readonly LoanDbContext _dbContext;

        public StatsController(LoanDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet("loan")]
        
        public async Task<IActionResult> Loan()
        {
            using (var context = this._dbContext)
            {
                var draft = context.Loans.Where(c => !c.IsPipeline && c.ProcessStatus.Equals(Constants.LoanStatus.Draft));
                var pending = context.Loans.Where(c => !c.IsPipeline && c.ProcessStatus.Equals(Constants.LoanStatus.PendingToReview));
                var accepted = context.Loans.Where(c => !c.IsPipeline && c.ProcessStatus.Equals(Constants.LoanStatus.Accepted));

                var groupedLoansStatus = new Dictionary<string, int>();
                groupedLoansStatus.Add("Draft", draft.Count());
                groupedLoansStatus.Add("Pending", pending.Count());
                groupedLoansStatus.Add("Accepted", accepted.Count());

                var groupedLoanAmount = context.Loans.Where(c => !c.IsPipeline
                                                            || c.ProcessStatus.Equals(Constants.LoanStatus.Draft)
                                                            || c.ProcessStatus.Equals(Constants.LoanStatus.PendingToReview)
                                                            || c.ProcessStatus.Equals(Constants.LoanStatus.Accepted))
                                            .GroupBy(c => c.ProcessStatus)
                                            .Select(c => new
                                            {
                                                Key = c.Key,
                                                Total = c.Sum(p => p.ConstructionLoanRequest)
                                            }).ToList();

                return this.Ok(new
                {
                    groupedStatusLoans = groupedLoansStatus,
                    groupedLoanAmount= groupedLoanAmount
                });
            }
        }

        [HttpGet("pipeline")]
        public async Task<IActionResult> Pipeline()
        {
            using (var context = this._dbContext)
            {
                var accepted = context.Loans.Where(c => c.IsPipeline && c.TermSheetStatus.Equals(Constants.LoanStatus.Accepted));
                var acceptedWithDeposit = context.Loans.Where(c => c.IsPipeline && c.TermSheetStatus.Equals(Constants.LoanStatus.AcceptedWithDeposit));
                var expiredTermSheets = context.Loans.Where(c => c.IsPipeline && c.TermSheetStatus.Equals(Constants.LoanStatus.ExpiredTermSheet));
                var pendingTermSheets = context.Loans.Where(c => c.IsPipeline && c.TermSheetStatus.Equals(Constants.LoanStatus.PendingTermsheetSignature));
                var signedTermSheets = context.Loans.Where(c => c.IsPipeline && c.TermSheetStatus.Equals(Constants.LoanStatus.SignedTermSheet));

                var groupedTermSheetStatus = new Dictionary<string, int>();
                groupedTermSheetStatus.Add("Expired", expiredTermSheets.Count());
                groupedTermSheetStatus.Add("Pending Signature", pendingTermSheets.Count());
                groupedTermSheetStatus.Add("Signed", signedTermSheets.Count());

                var groupedLoanStatus = new Dictionary<string, int>();
                groupedLoanStatus.Add("Accepted", accepted.Count());
                groupedLoanStatus.Add("Accepted W/Deposit", acceptedWithDeposit.Count());


                return this.Ok(new
                {
                    groupedTermSheetStatus = groupedTermSheetStatus,
                    groupedLoanStatus = groupedLoanStatus,
                });
            }
        }

    }
}

