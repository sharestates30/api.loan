﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;

namespace Api.Controllers
{
    public partial class LoanController
    {
        [HttpPost("{id}/assignTo")]
        public async Task<IActionResult> AssignTo(Guid id, [FromBody]LoanAssignToPostViewModel model)
        {

            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {

                var entity = await context.Loans.FirstOrDefaultAsync(c => c.LoanId.Equals(id));
                    
                //entity.OwnerUserId = model.OwnerUserId;
                //entity.OwnerUserName = model.OwnerUserName;

                // Add Tracking
                //entity.AddTracking(entity.Status.ToString(), this.SubjectId, this.SubjectName);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanAssignUserQueue, new
                {
                    LoanId = entity.LoanId,
                    UserId = model.OwnerUserId,
                    UserName = model.OwnerUserName
                });

                await this._queueService.SendMessage(Constants.QueueNames.LoanCreateNotificationQueue, new
                {
                    LoanId = entity.LoanId,
                    UserId = model.OwnerUserId,
                    UserName = model.OwnerUserName,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    Content = model.Comment
                });

                return this.Ok();
            }
        }

        [HttpPost("{id}/attorneyAssign")]
        public async Task<IActionResult> AssignVendorTo(Guid id, [FromBody]LoanAttorneyUserToPostViewModel model)
        {

            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {

                var entity = await context.Loans.FirstOrDefaultAsync(c => c.LoanId.Equals(id));

                //entity.AssingVendor(model.AttorneyUserId, model.AttorneyUserName, this.SubjectId, this.SubjectName);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();

                await this._queueService.SendMessage(Constants.QueueNames.LoanAssignUserQueue, new
                {
                    LoanId = entity.LoanId,
                    UserId = model.AttorneyUserId,
                    UserName = model.AttorneyUserName
                });

                await this._queueService.SendMessage(Constants.QueueNames.LoanCreateNotificationQueue, new
                {
                    LoanId = entity.LoanId,
                    UserId = model.AttorneyUserId,
                    UserName = model.AttorneyUserName,
                    DeveloperId = entity.BorrowerId,
                    DeveloperName = entity.BrokerEntityName,
                    BrokerId = entity.BrokerId,
                    BrokerName = entity.BrokerEntityName,
                    Content = model.Comment
                });

                return this.Ok();
            }
        }

    }
}

