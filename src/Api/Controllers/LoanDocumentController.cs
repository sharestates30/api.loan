﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    public partial class LoanController
    {

        [HttpGet("{id}/documents")]
        public async Task<IActionResult> GetDocuments(Guid id)
        {
            using (var context = this._dbContext)
            {
                var model = await (from ld in context.LoanDocuments
                             join d in context.Documents on ld.DocumentId equals d.DocumentId
                             join ldf in context.LoanDocumentFiles on ld.LoanDocumentId equals ldf.LoanDocumentId into g
                             from t in g.DefaultIfEmpty()
                             where ld.LoanId.Equals(id)
                             orderby d.Order
                             select new LoanDocumentViewModel {
                                 LoanId = ld.LoanId,
                                 DocumentCode = d.DocumentCode,
                                 DocumentName = d.Name,
                                 Status = (int)ld.Status,
                                 NeedValidation = d.NeedValidation,
                                 InternalDocument = (int)d.InternalDocument,
                                 AllowShared = d.AllowShared,
                                 Order = d.Order,
                                 Files = g.Select(p=> new LoanDocumentFileViewModel
                                 {
                                     FileId = p.FileId,
                                     FileLink = p.FileLink,
                                     FileName = p.FileName,
                                     CreatedBy = p.CreatedBy,
                                     CreatedByName = p.CreatedByName,
                                     CreatedOn  = p.CreatedOn
                                 })
                             }).ToListAsync();

                return this.Ok(model);
            }
        }

        [HttpGet("{id}/documents/findByCode")]
        public async Task<IActionResult> GetDocumentsByDocumentCode(Guid id, [FromQuery(Name = "documentCode")]string documentCode)
        {
            var documentCodes = documentCode.Split(',');

            if (!documentCodes.Any())
                return this.BadRequest();

            using (var context = this._dbContext)
            {
                var model = await (from ld in context.LoanDocuments
                                   join d in context.Documents on ld.DocumentId equals d.DocumentId
                                   join ldf in context.LoanDocumentFiles on ld.LoanDocumentId equals ldf.LoanDocumentId into g
                                   from t in g.DefaultIfEmpty()
                                   where ld.LoanId.Equals(id) && documentCodes.Contains(d.DocumentCode)
                                   orderby d.Order
                                   select new LoanDocumentViewModel
                                   {
                                       LoanId = ld.LoanId,
                                       DocumentCode = d.DocumentCode,
                                       DocumentName = d.Name,
                                       Status = (int)ld.Status,
                                       NeedValidation = d.NeedValidation,
                                       InternalDocument = (int)d.InternalDocument,
                                       AllowShared = d.AllowShared,
                                       Order = d.Order,
                                       Files = g.Select(p => new LoanDocumentFileViewModel
                                       {
                                           FileId = p.FileId,
                                           FileLink = p.FileLink,
                                           FileName = p.FileName,
                                           CreatedBy = p.CreatedBy,
                                           CreatedByName = p.CreatedByName,
                                           CreatedOn = p.CreatedOn
                                       })
                                   }).ToListAsync();

                return this.Ok(model);
            }
        }

        [HttpPut("{id}/documents/{documentCode}/{fileId}")]
        public async Task<IActionResult> ChangeStatusDocuments(Guid id, string documentCode, string fileId, [FromBody] LoandChangeDocumentStatusPostViewModel model) {

            using (var context = this._dbContext)
            {
                var entity = await context
                                .LoanDocuments
                                    .Include(c=> c.Loan)
                                    .Include(c=> c.Files)
                                .FirstOrDefaultAsync(c => c.LoanId.Equals(id) && c.Document.DocumentCode.Equals(documentCode, StringComparison.OrdinalIgnoreCase));

                if (entity != null) {

                    entity.Status = (DocumentStatusTypeEnum)model.Status;

                    if (model.Files != null && model.Files.Any()) {

                        foreach (var item in model.Files)
                        {
                            entity.AddFile(item.FileId, item.FileName, item.FileLink, this.SubjectId, this.SubjectName);
                        }

                    }

                    //if (!string.IsNullOrEmpty(model.Comment))
                    //    entity.Loan.AddComments(model.Comment, this.SubjectId, this.SubjectName);

                    context.LoanDocuments.Update(entity);
                    await context.SaveChangesAsync();

                    await this._queueService.SendMessage(Constants.QueueNames.LoanDocumentChangeStatusQueue, new
                    {
                        LoanId = entity.LoanId,
                        DeveloperId = entity.Loan.BorrowerId,
                        DeveloperName = entity.Loan.BrokerEntityName,
                        BrokerId = entity.Loan.BrokerId,
                        BrokerName = entity.Loan.BrokerEntityName,
                        IsBorrower = entity.Loan.BrokerId.HasValue ? false : true,
                        Status = (int)entity.Status,
                        StatusName = entity.Status.ToString()
                    });

                }

                return this.Ok();
            }

        }
        
    }
}

