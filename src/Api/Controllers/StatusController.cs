﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using Core;
using Microsoft.EntityFrameworkCore;

namespace Api.Controllers
{

    [Route("[controller]")]
    public class StatusController : Controller
    {

        private readonly LoanDbContext _dbContext;

        public StatusController(LoanDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {

            var codeLists = new List<string>()
            {
                Constants.ParameterGroup.OpenerStatusProject,
                Constants.ParameterGroup.OriginatorStatusProject,
                Constants.ParameterGroup.ProcessingDepartmentStatusProject,
                Constants.ParameterGroup.UnderwritingStatusProject,
                Constants.ParameterGroup.ClosingDeskStatusProject,
                Constants.ParameterGroup.PostClosingStatusProject,
            };

            var statusGroups = new Dictionary<string, string>();
            statusGroups.Add("Openers", Constants.ParameterGroup.OpenerStatusProject);
            statusGroups.Add("Originations", Constants.ParameterGroup.OriginatorStatusProject);
            statusGroups.Add("ProcessingDepartment", Constants.ParameterGroup.ProcessingDepartmentStatusProject);
            statusGroups.Add("Underwriting", Constants.ParameterGroup.UnderwritingStatusProject);
            statusGroups.Add("ClosingDesk", Constants.ParameterGroup.ClosingDeskStatusProject);
            statusGroups.Add("PostClosing", Constants.ParameterGroup.PostClosingStatusProject);

            var status = await this._dbContext.Parameters.Where(c => codeLists.Contains(c.Code)).ToListAsync();

            var model = new List<StatusGroupViewModel>();

            foreach (var item in statusGroups)
            {
                var group = new StatusGroupViewModel();
                group.Group = item.Key;
                group.Status = status.Where(c => c.Code.Equals(item.Value)).Select(c => new StatusViewModel { Value = c.Value, Name = c.Name }).ToList();

                model.Add(group);
            }

            return this.Ok(model);
        }


    }
}
