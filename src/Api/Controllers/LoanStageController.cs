﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Api.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    public partial class LoanController : BaseController
    {

        [HttpGet("v1/find")]
        public async Task<IActionResult> GetByV2([FromQuery(Name = "stageCode")]string stageCode, [FromQuery(Name = "stageGroupCode")]string stageGroupCode)
        {
            var processStatus = this.User.AllowFilterStatus(stageCode, stageGroupCode, this._dbContext);
            var assignedOwnerId = this.User.GetAssignerOwner();

            using (var context = this._dbContext)
            {
                var model = (from c in context.Loans
                             orderby c.SubmittedDay descending
                             select new LoanListViewModel
                             {
                                 LoanId = c.LoanId,
                                 LoanNumber = c.LoanNumber,
                                 TransactionType = (int)c.TransactionType,
                                 TransactionTypeName = c.TransactionType.ToString(),
                                 LoanPurposeType = (int)c.LoanPurposeType,
                                 LoanPurposeTypeName = c.LoanPurposeType.ToString(),
                                 PurchasePrice = c.PurchasePrice,
                                 ConstructionBudget = c.ConstructionBudget,
                                 ConstructionLoanRequest = c.ConstructionLoanRequest,
                                 AquisitionLoanRequest = c.AquisitionLoanRequest,
                                 LoanTermRequest = c.LoanTermRequest,
                                 HasRehabComponent = c.HasRehabComponent,
                                 HasSalesRepresentative = c.HasSalesRepresentative,
                                 IsApproved = c.IsApproved,
                                 IsPipeline = c.IsPipeline,
                                 OriginalPurchaseDate = c.OriginalPurchaseDate,
                                 OriginalPurchasePrice = c.OriginalPurchasePrice,
                                 AsIsValue = c.AsIsValue,
                                 FinalValue = c.FinalValue,
                                 DesiredFundingDate = c.DesiredFundingDate,
                                 Address = c.Address,
                                 Country = c.Country,
                                 City = c.City,
                                 State = c.State,
                                 ZipCode = c.ZipCode,
                                 BorrowerName = c.BorrowerName,
                                 BrokerEntityName = c.BrokerEntityName,
                                 BorrowerCreditScore = c.BorrowerCreditScore,
                                 BorrowerId = c.BorrowerId,
                                 BrokerId = c.BrokerId,
                                 SubmittedDay = c.SubmittedDay,
                                 ProcessStatus = c.ProcessStatus,
                                 BrokerAgentName = c.BrokerAgentName,
                                 BorrowerEntityName = c.BorrowerEntityName,
                                 TermSheetStatus = c.TermSheetStatus,
                                 CreatedOn = c.CreatedOn
                             });

                //if (!string.IsNullOrEmpty(assignedOwnerId))
                //{
                //    model = model.Where(c => c.OwnerUserId.Equals(assignedOwnerId));
                //}

                if (processStatus != null && processStatus.Any()) {
                    model = model.Where(c => processStatus.Contains(c.ProcessStatus));
                }

                var result = await model.ToListAsync();
                var status = await this._loanStatus.GetStatus();

                foreach (var item in result)
                {
                    var statusValue = status.FirstOrDefault(c => c.Value.Equals(item.ProcessStatus));
                    var termSheetStatusValue = status.FirstOrDefault(c => c.Value.Equals(item.TermSheetStatus));
                    if (statusValue != null)
                    {
                        item.ProcessStatusName = statusValue.Name;
                    }

                    if (termSheetStatusValue != null)
                    {
                        item.TermSheetStatusName = termSheetStatusValue.Name;
                    }
                }


                return this.Ok(result);
            }
        }
        
    }
}

