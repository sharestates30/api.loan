﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Api.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    [Route("pipeline")]
    [Authorize]
    public partial class LoanPipelineController : BaseController
    {
        private readonly LoanDbContext _dbContext;
        private readonly IHostingEnvironment _env;
        private readonly ISerializeDataService _serializeDataService;
        private readonly IQueueService _queueService;
        private readonly ILoanStatus _loanStatus;

        public LoanPipelineController(LoanDbContext dbContext, IHostingEnvironment env, ISerializeDataService serializeDataService, IQueueService queueService, ILoanStatus loanStatus)
        {
            this._dbContext = dbContext;
            this._env = env;
            this._serializeDataService = serializeDataService;
            this._queueService = queueService;
            this._loanStatus = loanStatus;
        }
        
    }
}

