﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;

namespace Api.Controllers
{
    public partial class LoanController
    {

        [HttpGet("{id}/termSheet")]
        public async Task<IActionResult> GetTermSheet(Guid id)
        {
            using (var context = this._dbContext)
            {
                var model = await context
                                .Loans
                                    .Include(c=> c.TermSheet)
                                .Where(c => c.LoanId.Equals(id))
                                .Select(c => new LoanTermSheetViewModel
                                {
                                    LoanId = c.LoanId,
                                    BorrowerName = c.BorrowerName,
                                    LoanPurposeType = (int)c.LoanPurposeType,
                                    LoanPurposeTypeName = c.LoanPurposeType.ToString(),
                                    Property = c.Address,
                                    LoanTermRequest = c.LoanTermRequest,
                                    LoanRequest2 = c.ConstructionLoanRequest,
                                    PurchasePrice = c.PurchasePrice,
                                    Lender = c.TermSheet != null ? c.TermSheet.Lender : string.Empty,
                                    ExtensionOptions = c.TermSheet != null ? c.TermSheet.ExtensionOptions : string.Empty,
                                    EarlyPayoff = c.TermSheet != null ? c.TermSheet.EarlyPayoff : 0,
                                    Assumability = c.TermSheet != null ? c.TermSheet.Assumability : 0,
                                    Rate = c.TermSheet != null ? c.TermSheet.Rate : 0,
                                    Points = c.TermSheet != null ? c.TermSheet.Points : 0,
                                    InterestAccrual = c.TermSheet != null ? c.TermSheet.InterestAccrual : 0,
                                    AmountExistingLiens = c.TermSheet != null ? c.TermSheet.AmountExistingLiens : 0,
                                    PurposeRefinance = c.TermSheet != null ? c.TermSheet.PurposeRefinance : 0,
                                    DownPayment = c.TermSheet != null ? c.TermSheet.DownPayment : 0,
                                    RehabCost = c.TermSheet != null ? c.TermSheet.RehabCost : 0,
                                    PercentageRehabCostFinanced = c.TermSheet != null ? c.TermSheet.PercentageClosingCostsFinanced : 0,
                                    RehabCostFinanced = c.TermSheet != null ? c.TermSheet.RehabCost : 0,
                                    LTV = c.TermSheet != null ? c.TermSheet.LTV : 0,
                                    InvestmentsOriginationPoints = c.TermSheet != null ? c.TermSheet.InvestmentsOriginationPoints : 0,
                                    BrokerFee = c.TermSheet != null ? c.TermSheet.BrokerFee : 0,
                                    BankAttorneyFee = c.TermSheet != null ? c.TermSheet.BankAttorneyFee : 0,
                                    ConstructionDraws = c.TermSheet != null ? c.TermSheet.ConstructionDraws : 0,
                                    EstimatedAppraisal = c.TermSheet != null ? c.TermSheet.EstimatedAppraisal : 0,
                                    TermSheetBinding = c.TermSheet != null ? c.TermSheet.TermSheetBinding : 0,
                                    EstimatedTitleClosingCosts = c.TermSheet != null ? c.TermSheet.EstimatedTitleClosingCosts : 0,
                                    PercentageClosingCostsFinanced = c.TermSheet != null ? c.TermSheet.PercentageClosingCostsFinanced : 0,
                                    ClosingCostsFinanced = c.TermSheet != null ? c.TermSheet.ClosingCostsFinanced : 0,
                                    InterestReserve = c.TermSheet != null ? c.TermSheet.InterestReserve : 0,
                                    RefinanceClosingCostsFinanced = c.TermSheet != null ? c.TermSheet.RefinanceClosingCostsFinanced : 0,
                                    LoanAmountMayNotExceedARV = c.TermSheet != null ? c.TermSheet.LoanAmountMayNotExceedARV : 0,
                                    AsIsValueNeedfromSharestatesOrderedAppraiser = c.TermSheet != null ? c.TermSheet.AsIsValueNeedfromSharestatesOrderedAppraiser : 0,
                                    LoanAmountMayNotExceedAsIsValue = c.TermSheet != null ? c.TermSheet.LoanAmountMayNotExceedAsIsValue : 0,
                                    Guarantor = c.TermSheet != null ? c.TermSheet.Guarantor : string.Empty,
                                    BorrowerForm = c.TermSheet != null ? c.TermSheet.BorrowerForm : string.Empty,
                                    Payments = c.TermSheet != null ? c.TermSheet.Payments : 0,
                                    SingleEntity = c.TermSheet != null ? c.TermSheet.SingleEntity : string.Empty,
                                    CrossDefault = c.TermSheet != null ? c.TermSheet.CrossDefault : string.Empty,
                                    Collateral = c.TermSheet != null ? c.TermSheet.Collateral : string.Empty,
                                    SecondaryFinance = c.TermSheet != null ? c.TermSheet.SecondaryFinance : string.Empty,
                                    TitleInsurance = c.TermSheet != null ? c.TermSheet.TitleInsurance : string.Empty,
                                    PowerAttorney = c.TermSheet != null ? c.TermSheet.PowerAttorney : string.Empty,
                                    HazardInsurance = c.TermSheet != null ? c.TermSheet.HazardInsurance : string.Empty
                                }).FirstOrDefaultAsync();

                return this.Ok(model);
            }
        }

        [HttpPost("{id}/termSheet")]
        public async Task<IActionResult> PostTermSheet(Guid id, [FromBody] LoanTermSheetPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = await context
                                .TermSheets
                                .FirstOrDefaultAsync(c => c.LoanId.Equals(id));

                var isNew = false;
                if (entity == null) {

                    entity = new TermSheet();
                    entity.LoanId = id;
                    isNew = true;
                }

                entity.Lender = model.Lender;
                entity.ExtensionOptions = model.ExtensionOptions;
                entity.EarlyPayoff = model.EarlyPayoff;
                entity.Assumability = model.Assumability;
                entity.Rate = model.Rate;
                entity.Points = model.Points;
                entity.InterestAccrual = model.InterestAccrual;
                entity.AmountExistingLiens = model.AmountExistingLiens;
                entity.PurposeRefinance = model.PurposeRefinance;
                entity.DownPayment = model.DownPayment;
                entity.RehabCost = model.RehabCost;
                entity.PercentageRehabCostFinanced = model.PercentageClosingCostsFinanced;
                entity.RehabCostFinanced = model.RehabCost;
                entity.LTV = model.LTV;
                entity.InvestmentsOriginationPoints = model.InvestmentsOriginationPoints;
                entity.BrokerFee = model.BrokerFee;
                entity.BankAttorneyFee = model.BankAttorneyFee;
                entity.ConstructionDraws = model.ConstructionDraws;
                entity.EstimatedAppraisal = model.EstimatedAppraisal;
                entity.TermSheetBinding = model.TermSheetBinding;
                entity.EstimatedTitleClosingCosts = model.EstimatedTitleClosingCosts;
                entity.PercentageClosingCostsFinanced = model.PercentageClosingCostsFinanced;
                entity.ClosingCostsFinanced = model.ClosingCostsFinanced;
                entity.InterestReserve = model.InterestReserve;
                entity.RefinanceClosingCostsFinanced = model.RefinanceClosingCostsFinanced;
                entity.LoanAmountMayNotExceedARV = model.LoanAmountMayNotExceedARV;
                entity.AsIsValueNeedfromSharestatesOrderedAppraiser = model.AsIsValueNeedfromSharestatesOrderedAppraiser;
                entity.LoanAmountMayNotExceedAsIsValue = model.LoanAmountMayNotExceedAsIsValue;
                entity.Guarantor = model.Guarantor;
                entity.BorrowerForm = model.BorrowerForm;
                entity.Payments = model.Payments;
                entity.SingleEntity = model.SingleEntity;
                entity.CrossDefault = model.CrossDefault;
                entity.Collateral = model.Collateral;
                entity.SecondaryFinance = model.SecondaryFinance;
                entity.TitleInsurance = model.TitleInsurance;
                entity.PowerAttorney = model.PowerAttorney;
                entity.HazardInsurance = model.HazardInsurance;

                if (isNew)
                {
                    entity.CreatedBy = this.SubjectId;
                    entity.CreatedByName = this.SubjectName;
                    entity.CreatedOn = DateTime.UtcNow;

                    context.TermSheets.Add(entity);
                }
                else {
                    entity.UpdatedBy = this.SubjectId;
                    entity.UpdatedByName = this.SubjectName;
                    entity.UpdatedOn = DateTime.UtcNow;

                    context.TermSheets.Update(entity);
                }

                await context.SaveChangesAsync();

                return this.Ok();
            }
        }

    }
}

