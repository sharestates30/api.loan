﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using Core;

namespace Api.Controllers
{
    [Route("[controller]")]
    public class ParameterController : Controller
    {
        private readonly LoanDbContext _dbContext;

        public ParameterController(LoanDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        [HttpGet("groups")]
        public IActionResult GetGroups()
        {
            var model = new List<ParameterGroupViewModel>();
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.BorrowerCreditScore, Name = "Credit Score" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.HearAboutUs, Name = "Hear About Us" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.RefinanceHolderDuration, Name = "Refinance Holder Duration" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.RefinanceMortgagesDefaultReason, Name = "Refinance Mortgages Default Reason" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.RefinancePropertyWorkRange, Name = "Refinance Property Work Range" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.YearsInRealestate, Name = "Years In Real estate" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.BorrowerTransactionTotalAmount, Name = "Borrower Transaction Total Amount" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.OpenerStatusProject, Name = "Opener Pipeline Status" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.OriginatorStatusProject, Name = "Originator Pipeline Status" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.ProcessingDepartmentStatusProject, Name = "Processing department Pipeline Status" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.UnderwritingStatusProject, Name = "Underwriter Pipeline Status" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.ClosingDeskStatusProject, Name = "Closing Desk Pipeline Status" });
            model.Add(new ParameterGroupViewModel { Id = Constants.ParameterGroup.PostClosingStatusProject, Name = "Post Closing Pipeline Status" });

            return this.Ok(model);
        }

        [HttpGet]
        public IActionResult Get([FromQuery(Name = "code")]string code)
        {
            using (var context = this._dbContext)
            {
                var model = (from c in context.Parameters
                             select new ParameterViewModel
                             {
                                 ParameterId = c.ParameterId,
                                 Code = c.Code,
                                 Name = c.Name,
                                 Value = c.Value,
                                 IsActive = c.IsActive
                             });

                if (!string.IsNullOrEmpty(code))
                {
                    var codeList = code.Split(',');
                    model = model.Where(c => codeList.Contains(c.Code));
                }

                return this.Ok(model.ToList());
            }
        }

        [HttpGet("{id}", Name = "GetParameterById")]
        public IActionResult GetById(int id)
        {
            using (var context = this._dbContext)
            {
                var model = (from c in context.Parameters
                             where c.ParameterId.Equals(id)
                             select new ParameterViewModel
                             {
                                 ParameterId = c.ParameterId,
                                 Code = c.Code,
                                 Name = c.Name,
                                 Value = c.Value,
                                 IsActive = c.IsActive
                             }).FirstOrDefault();

                if (model == null)
                    return this.NotFound();

                return this.Ok(model);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ParameterPostViewModel model)
        {
            if (!ModelState.IsValid)
                return this.BadRequest(ModelState);

            using (var context = this._dbContext)
            {
                var entity = new Parameter
                {
                    Code = model.Code,
                    Name = model.Name,
                    Value = model.Value,
                    IsActive = model.IsActive
                };

                context.Parameters.Add(entity);
                await context.SaveChangesAsync();

                return this.Created(Url.RouteUrl("GetParameterById", new { id = entity.ParameterId }), new { });
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]ParameterPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context.Parameters.FirstOrDefault(c => c.ParameterId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                entity.Code = model.Code;
                entity.Name = model.Name;
                entity.Value = model.Value;
                entity.IsActive = model.IsActive;

                context.Parameters.Update(entity);
                await context.SaveChangesAsync();

                return this.Ok();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            using (var context = this._dbContext)
            {
                var entity = context.Parameters.FirstOrDefault(c => c.ParameterId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                context.Parameters.Remove(entity);
                await context.SaveChangesAsync();

                return this.NoContent();
            }
        }
    }
}

