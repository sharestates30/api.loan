﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entity;
using Api.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Core;
using Microsoft.EntityFrameworkCore;
using Api.Services;
using Microsoft.AspNetCore.Authorization;

namespace Api.Controllers
{
    public partial class LoanController
    {

        [HttpPost("{id}/transactions")]
        public async Task<IActionResult> AddTransactions(Guid id, [FromBody] LoanTransactionPostViewModel model)
        {
            using (var context = this._dbContext)
            {
                var entity = context
                                .Loans
                                .Include(c => c.Transactions)
                                .FirstOrDefault(c => c.LoanId.Equals(id));

                if (entity == null)
                    return this.NotFound();

                var currentLoanSerialize = this._serializeDataService.SerializeLoan(entity);

                entity.UpdatedBy = this.SubjectId;
                entity.UpdatedByName = this.SubjectName;
                entity.UpdatedOn = DateTime.UtcNow;

                var newLoanSerialize = this._serializeDataService.SerializeLoan(entity);
                entity.AddLog(Constants.ActionTypeName.Comment, newLoanSerialize, currentLoanSerialize, this.SubjectId, this.SubjectName);
                entity.AddTransaction(model.TransactionId, this.SubjectId, this.SubjectName);

                context.Loans.Update(entity);
                await context.SaveChangesAsync();
                
                return this.NoContent();
            }
        }

        [HttpGet("{id}/transactions")]
        public async Task<IActionResult> GetTransactions(Guid id)
        {
            using (var context = this._dbContext)
            {
                var model = context
                                .LoanTransactions
                                .Include(c => c.Loan)
                                .Where(c => c.LoanId.Equals(id))
                                .Select(c => new LoanTransactionViewModel
                                {
                                    LoanId = c.LoanId,
                                    TransactionId = c.TransactionId,
                                    LoanTransactionId = c.LoanTransactionId,
                                    CreatedBy = c.CreatedBy,
                                    CreatedByName = c.CreatedByName,
                                    CreatedOn = c.CreatedOn,
                                    TransactionDate = c.TransactionDate,
                                    TransactionStatus = c.TransactionStatus
                                });

                var result = await model.OrderByDescending(c => c.CreatedOn).ToListAsync();

                return this.Ok(result);
            }
        }
        
    }
}

