﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    public class BaseController : Controller
    {

        public string SubjectId
        {
            get
            {
                var sub = this.User.Claims.FirstOrDefault(c => c.Type == "sub");

                if (sub != null)
                    return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;

                return "sharestates";
            }
        }

        public string SubjectName
        {
            get
            {
                var name = this.User.Claims.FirstOrDefault(c => c.Type == "full_name");

                if (name != null)
                    return name.Value;

                return "sharestates";
            }
        }
    }
}
