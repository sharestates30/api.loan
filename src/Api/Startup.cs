﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Entity.Configuration;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.Swagger.Model;
using Api.Services;
using IdentityServer4;

namespace Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddCors();
            services.AddMvc();

            // Add Api Docs.
            //var pathToDoc = $"{PlatformServices.Default.Application.ApplicationBasePath}\\Api.xml";
            //services.AddSwaggerGen();
            //services.ConfigureSwaggerGen(options =>
            //{
            //    options.SingleApiVersion(new Info
            //    {
            //        Version = "v1",
            //        Title = "API",
            //        Description = "Api",
            //        TermsOfService = "None"
            //    });
            //    options.IncludeXmlComments(pathToDoc);
            //    options.DescribeAllEnumsAsStrings();
            //});
//
            // AppSettings.
            services.AddDbContext<LoanDbContext>(options => options.UseSqlServer(Configuration["Data:ConnectionString"]));

            services.AddTransient<ISerializeDataService, SerializeDataService>();
            services.AddTransient<IQueueService, QueueService>();
            services.AddTransient<ILoanStatus, LoanStatus>();
            services.AddTransient<IViewRenderService, ViewRenderService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseStaticFiles();
            app.UseDeveloperExceptionPage();
            
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = Configuration.GetValue<string>("Authority"),
                RequireHttpsMetadata = false,
                AllowedScopes = {
                    "api",
                    IdentityServerConstants.StandardScopes.OpenId,
                    IdentityServerConstants.StandardScopes.Profile,
                    IdentityServerConstants.StandardScopes.OfflineAccess },
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
            });
            app.UseMvc();

            //app.UseSwagger();
            //app.UseSwaggerUi();
        }
    }
}
