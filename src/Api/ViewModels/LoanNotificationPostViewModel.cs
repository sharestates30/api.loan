﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanNotificationPostViewModel
    {
        [Required]
        public Guid UserId { get; set; }
        [Required]
        public string UserName { get; set; }
        public int? NotificationParentId { get; set; }
        [Required]
        public string Content { get; set; }
    }
}
