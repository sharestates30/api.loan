﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entity;

namespace Api.ViewModels
{
    public class LoanInsuranceViewModel
    {
        public Guid LoanId { get; set; }
        public bool IsHazardInsuranceRequired { get; set; }
        public string HazardInsurancePolicyFileId { get; set; }
        public string HazardInsurancePolicyFileName { get; set; }
        public string HazardInsurancePolicyFileLink { get; set; }

        public decimal? HazardInsuranceAmount { get; set; }
        public DateTime? HazardInsuranceGoodThroughDate { get; set; }

        public string HazardInsurancePaidReceiptFileId { get; set; }
        public string HazardInsurancePaidReceiptFileName { get; set; }
        public string HazardInsurancePaidReceiptFileLink { get; set; }

        public bool IsFloodInsuranceRequired { get; set; }

        public string FloodInsurancePolicyFileId { get; set; }
        public string FloodInsurancePolicyFileName { get; set; }
        public string FloodInsurancePolicyFileLink { get; set; }

        public decimal? FloodInsuranceAmount { get; set; }
        public DateTime? FloodInsuranceGoodThroughDate { get; set; }

        public string FloodInsurancePaidReceiptFileId { get; set; }
        public string FloodInsurancePaidReceiptFileName { get; set; }
        public string FloodInsurancePaidReceiptFileLink { get; set; }
    }
}
