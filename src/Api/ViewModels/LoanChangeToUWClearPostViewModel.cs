﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToUWClearPostViewModel
    {
        public Guid TitleCompanyId { get; set; }
        public string TitlePropertyNumber { get; set; }
        public string TitlePropertyBusinessName { get; set; }
        public string TitlePropertyFirstName { get; set; }
        public string TitlePropertyLastName { get; set; }
        public string Comment { get; set; }
        public DateTime TitleReportOrderedDate { get; set; }
        public DateTime TitleReportBackDate { get; set; }
        public string TitleReportStatus { get; internal set; }
    }
}
