﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToLenderInstructionsIssuedPostViewModel
    {

        /// <summary>
        /// Documents : Lender’s Letter
        /// </summary>
        public string LenderLetterFileId { get; set; }
        public string LenderLetterFileName { get; set; }
        public string LenderLetterFileLink { get; set; }

        public string Comment { get; set; }
    }
}
