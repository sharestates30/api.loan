﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanBidPostViewModel
    {
        public Guid VendorId { get; set; }
        public string VendorName { get; set; }
        public string Comment { get; set; }

        public decimal Bid { get; set; }
        public int AuctionType { get; set; }
    }

    public class LoanAwardedPostViewModel
    {
        public string Comment { get; set; }
    }

}
