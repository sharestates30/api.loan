﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanChangeToAppraisalReceivedPostViewModel
    {
        [Required]
        public string AppraisalCompanyName { get; set; }
        [Required]
        public decimal AppraiserFee { get; set; }
        [Required]
        public DateTime AppraisalDateOrdered { get; set; }
        [Required]
        public DateTime AppraisalDateNeeded { get; set; }
        [Required]
        public string AppraisalStatus { get; set; }

        public bool Approve { get; set; }

        public string Comment { get; set; }
    }

    public class LoanChangeToAppraisalOrderedPostViewModel
    {
        /// <summary>
        /// Documents : Appraisal
        /// </summary>
        [Required]
        public string AppraisalFileId { get; set; }
        [Required]
        public string AppraisalFileName { get; set; }
        [Required]
        public string AppraisalFileLink { get; set; }

        /// <summary>
        /// Documents : Appraisal Invoice
        /// </summary>
        [Required]
        public string AppraisalInvoiceFileId { get; set; }
        [Required]
        public string AppraisalInvoiceFileName { get; set; }
        [Required]
        public string AppraisalInvoiceFileLink { get; set; }

        public bool Approve { get; set; }

        public string Comment { get; set; }

    }

    public class LoanChangeToAppraisalReportSentPostViewModel
    {
        public string Comment { get; set; }
        public bool Approve { get; set; }
    }
}
