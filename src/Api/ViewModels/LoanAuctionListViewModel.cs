﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanAuctionListViewModel
    {
        public Guid LoanId { get; set; }
        public string LoanNumber { get; set; }
        public decimal LoanRequest1 { get; set; }
        public decimal LoanRequest2 { get; set; }
		public string Country { get; set; }
		public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        
        public DateTime CreatedOn { get; set; }
        public bool AuctionClosedForAppraiser { get; set; }
        public bool AuctionClosedForAttorney { get; set; }
        public bool AuctionClosedForTitleCompany { get; set; }
        public DateTime? TargetClosingDate { get; set; }
    }
}
