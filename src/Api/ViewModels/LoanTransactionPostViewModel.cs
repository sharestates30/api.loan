﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModels
{
    public class LoanTransactionPostViewModel
    {
        public string TransactionId { get; set; }
    }
}
