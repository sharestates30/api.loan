﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bus.Models;
using System.Net.Http;
using Sharestates.Util;

namespace Bus.Services
{
    public class LoanResolverService : ILoanResolverService
    {
        public async Task<LoanInfoViewModel> GetLoan(Guid loanId, OAuthToken token)
        {
            // Get api url
            var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
            var developerApi = System.Configuration.ConfigurationManager.AppSettings["developerApi"];
            var brokerApi = System.Configuration.ConfigurationManager.AppSettings["brokerApi"];

            var httpClient = new HttpClient();
            httpClient.SetBearerToken(token.access_token);

            // Send term sheet.
            var loanResponse = await httpClient.GetAsync($"{loanApi}/loan/{loanId}");
            var model = new LoanInfoViewModel();
            model.Loan = loanResponse.ReadAsObject<LoanViewModel>();

            if (model.Loan.BorrowerId.HasValue && !model.Loan.BorrowerId.Value.Equals(Guid.Empty))
            {
                var developerResponse = await httpClient.GetAsync($"{developerApi}/developer/{model.Loan.BorrowerId.Value}");

                model.Developer = developerResponse.ReadAsObject<DeveloperViewModel>();
            }

            if (model.Loan.BrokerId.HasValue && !model.Loan.BrokerId.Value.Equals(Guid.Empty)) {
                var brokerResponse = await httpClient.GetAsync($"{brokerApi}/broker/{model.Loan.BrokerId.Value}");

                model.Broker = brokerResponse.ReadAsObject<BrokerViewModel>();
            }

            return model;
        }

        public async Task<byte[]> GetLoanPdf(Guid loanId, OAuthToken token)
        {
            // Get api url
            var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];

            var loan = SharestatesConverter.ConvertUrlToPDF($"{loanApi}/loan/{loanId}?format=pdf");

            return loan;
        }
    }
}
