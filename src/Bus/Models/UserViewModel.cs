﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Avatar { get; set; }
        public bool IndividualUserType { get; set; }
        public bool BusinessUserType { get; set; }
        public bool DeveloperUserType { get; set; }
        public bool AdministratorUserType { get; set; }
        public bool VendorUserType { get; set; }
        public bool BrokerUserType { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string MobilePhone { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int Status { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
    }
}
