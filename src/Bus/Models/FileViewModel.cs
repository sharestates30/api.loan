﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class FileViewModel
    {
        public Guid FileItemId { get; set; }
        public string FileName { get; set; }
        public string FileUri { get; set; }
    }
}
