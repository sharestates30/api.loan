﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class LoanViewModel
    {
        public Guid LoanId { get; set; }
        public string LoanNumber { get; set; }
        public bool IsBorrower { get; set; }
        public int TransactionType { get; set; }
        public int LoanPurposeType { get; set; }
        public string LoanPurposeTypeName { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal ConstructionBudget { get; set; }
        public decimal LoanRequest1 { get; set; }
        public decimal LoanRequest2 { get; set; }
        public int LoanTermRequest { get; set; }
        public decimal AsIsValue { get; set; }
        public decimal AsIsValueEstimated { get; set; }
        public decimal FinalValue { get; set; }
        public DateTime DesiredFundingDate { get; set; }
        public bool ShortsaleTransactionType { get; set; }
        public bool AssignmentFlipType { get; set; }
        public bool LLCTransactionType { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public int Units { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public int AssetType { get; set; }
        public int AssetTypeUnits1 { get; set; }
        public int AssetTypeUnits2 { get; set; }
        public int DevelopmentPhase { get; set; }
        public int Occupancy { get; set; }
        public int ApprovedPlanType { get; set; }
        public string FileApprovedPlans { get; set; }
        public int InvestmentSummary { get; set; }
        public int ExitStrategy { get; set; }
        public string BorrowerName { get; set; }
        public string BrokerEntityName { get; set; }
        public int BorrowerCreditScore { get; set; }
        public bool ConvictedFelonyType { get; set; }
        public bool LawsuitsType { get; set; }
        public bool SECViolationsType { get; set; }
        public bool BankruptcyType { get; set; }
        public string BankruptcyExplanation { get; set; }
        public bool DefaultedLoanType { get; set; }
        public string DefaultedLoanExplanation { get; set; }
        public bool FiledForeclosure { get; set; }
        public string FiledForeclosureExplanation { get; set; }
        public bool OutstandingJudgmentsType { get; set; }
        public string OutstandingJudgmentsExplanation { get; set; }

        public string AppraisalCompanyName { get; set; }
        public string AppraiserName { get; set; }
        public string AppraiserAddress { get; set; }
        public string AppraiserCountry { get; set; }
        public string AppraiserCity { get; set; }
        public string AppraiserState { get; set; }
        public string AppraiserZipCode { get; set; }
        public string AppraiserEmail { get; set; }
        public string AppraiserPhone { get; set; }

        public string BuyerLawFirm { get; set; }
        public string BuyerAttorney { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerCountry { get; set; }
        public string BuyerCity { get; set; }
        public string BuyerState { get; set; }
        public string BuyerZipCode { get; set; }
        public string BuyerPhone { get; set; }
        public string BuyerEmail { get; set; }
        public string SellerLawFirm { get; set; }
        public string SellerAttorney { get; set; }
        public string SellerAddress { get; set; }
        public string SellerCountry { get; set; }
        public string SellerCity { get; set; }
        public string SellerState { get; set; }
        public string SellerZipCode { get; set; }
        public string SellerPhone { get; set; }
        public string SellerEmail { get; set; }
        public int HearAboutUs { get; set; }
        public string RefinancePropertyHolder { get; set; }
        public bool RefinanceSoleMemeberType { get; set; }
        public int RefinanceHolderDuration { get; set; }
        public bool RefinanceOpenMortgagesType { get; set; }
        public decimal RefinanceOpenMortgagesTotal { get; set; }
        public int RefinanceOpenMortgagesStatus { get; set; }
        public int RefinanceMortgagesDefaultReason { get; set; }
        public bool RefinancePropertyWorkType { get; set; }
        public int RefinancePropertyWorkRange { get; set; }
        public bool ShortSaleApprovedType { get; set; }
        public DateTime? ShortSaleExpireDate { get; set; }
        public string AssignFlipAmount { get; set; }
        public string AssignFlipEntityName { get; set; }
        public bool AssignFlipVestedInterestType { get; set; }
        public string AssignFlipVestedInterestEntity { get; set; }

        public int PropertyStatus { get; set; }

        public decimal PropertySellingPrice { get; set; }
        public DateTime? PropertySellingDate { get; set; }

        public DateTime? PropertyrefinanceDate { get; set; }
        public decimal PropertyAppraisedvalue { get; set; }
        public decimal LoanAmount { get; set; }

        public bool RepeatBorrower { get; set; }
        public int YearsInRealestate { get; set; }
        public string NoOfBorrowerTransaction { get; set; }
        public int BorrowerTransactionTotalAmount { get; set; }

        public int CurrentStep { get; set; }
        public string UserId { get; set; }
        public Guid? BorrowerId { get; set; }
        public Guid? SellerId { get; set; }
        public Guid? BuyerId { get; set; }
        public Guid? BrokerId { get; set; }
        public DateTime? SubmittedDay { get; set; }
        public string OwnerUserId { get; set; }
        public string OwnerUserName { get; set; }
        public string ProcessStatus { get; set; }
        public bool PostClosingComplete { get; set; }
        public bool ClosingDeskComplete { get; set; }
        public bool UnderwritingComplete { get; set; }
        public bool ProcessingDepartmentComplete { get; set; }
        public bool OriginationsComplete { get; set; }
        public string BrokerAgentName { get; set; }
        public object ProcessStatusName { get; set; }
        public string TermSheetStatus { get; set; }
        public string TermSheetStatusName { get; set; }
        public string BorrowerEntityName { get; set; }
        public Guid? AppraiserId { get; set; }
        public Guid? TitlePropertyId { get; internal set; }
        public Guid? MemberId { get; set; }
        public string MemberName { get; set; }
    }
}
