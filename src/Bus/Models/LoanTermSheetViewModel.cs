﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class LoanTermSheetViewModel
    {
        public Guid LoanId { get; set; }
        public string Lender { get; set; }
        public string ExtensionOptions { get; set; }
        public decimal EarlyPayoff { get; set; }
        public decimal Assumability { get; set; }
        public decimal Rate { get; set; }
        public decimal Points { get; set; }
        public decimal InterestAccrual { get; set; }
        public decimal PurchasePrice { get; set; }
        public decimal AmountExistingLiens { get; set; }
        public decimal PurposeRefinance { get; set; }
        public decimal DownPayment { get; set; }
        public decimal RehabCost { get; set; }
        public decimal PercentageRehabCostFinanced { get; set; }
        public decimal RehabCostFinanced { get; set; }
        /// <summary>
        /// Loan-to-Value (LTV)
        /// </summary>
        public decimal LTV { get; set; }
        public decimal InvestmentsOriginationPoints { get; set; }
        public decimal BrokerFee { get; set; }
        public decimal BankAttorneyFee { get; set; }
        public decimal ConstructionDraws { get; set; }
        /// <summary>
        /// Estimated Appraisal/Survey Fee
        /// </summary>
        public decimal EstimatedAppraisal { get; set; }

        /// <summary>
        /// Term Sheet Binding/Due Diligence Fee
        /// </summary>
        public decimal TermSheetBinding { get; set; }
        public decimal EstimatedTitleClosingCosts { get; set; }
        public decimal PercentageClosingCostsFinanced { get; set; }
        public decimal ClosingCostsFinanced { get; set; }
        public decimal InterestReserve { get; set; }
        public decimal RefinanceClosingCostsFinanced { get; set; }

        public decimal LoanAmountMayNotExceedARV { get; set; }
        public decimal AsIsValueNeedfromSharestatesOrderedAppraiser { get; set; }
        public decimal LoanAmountMayNotExceedAsIsValue { get; set; }

        public string Guarantor { get; set; }
        public string BorrowerForm { get; set; }
        public decimal Payments { get; set; }
        public string SingleEntity { get; set; }
        public string CrossDefault { get; set; }
        public string Collateral { get; set; }
        public string SecondaryFinance { get; set; }
        public string TitleInsurance { get; set; }
        public string PowerAttorney { get; set; }
        public string HazardInsurance { get; set; }
        public string BorrowerName { get; set; }
        public int LoanPurposeType { get; set; }
        public string Property { get; set; }
        public int LoanTermRequest { get; set; }
        public decimal LoanRequest1 { get; set; }
        public decimal LoanRequest2 { get; set; }
        public string LoanPurposeTypeName { get; set; }
    }
}
