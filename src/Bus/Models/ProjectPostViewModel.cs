﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Models
{
    public class ProjectPostViewModel
    {
        public string ProjectName { get; set; }
        public string AddressNumber { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Summary { get; set; }
        public string LoanNumber { get; set; }
        public string FundingTypeId { get; set; }
        public string LienPositionId { get; set; }
        public string PendingProjectAssetTypeId { get; set; }
        public string DevelopmentPhaseId { get; set; }
        public string OccupancyId { get; set; }

        public int RequestedTerm { get; set; }
        public decimal DesiredFundingAmount { get; set; }
        public decimal RequestedLoanAmount { get; set; }
        public decimal AcquisitionPrice { get; set; }
        public decimal AfterConstructionValue { get; set; }
        public DateTime DesiredFundingDate { get; set; }
        public string ApproximateCreditScoreId { get; set; }
        public string PendingProjectStatusId { get; set; }
        public string Stipulation { get; set; }
        public string AdminRoles { get; set; }
        public string LoanUnderwriting { get; set; }
        public string TitleUnderwriting { get; set; }
        public string LenderAttorney { get; set; }
        public string FundAdmin { get; set; }
        public string MarketSummary { get; set; }
        public string ExitStrategy { get; set; }
        public string PurchaseOrRefinanceId { get; set; }
        public string BrokerName { get; set; }
        public string BrokerId { get; set; }
        public string BuyerAttorneyId { get; set; }
        public string SellerAttorneyId { get; set; }
        public string BuyerAttorneyName { get; set; }
        public string SellerAttorneyName { get; set; }
        public string BorrowerId { get; set; }
        public string BorrowerName { get; set; }
        public string AppraiserName { get; set; }
        public string AppraiserId { get; set; }
    }
}
