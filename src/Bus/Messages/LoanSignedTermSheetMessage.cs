﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Messages
{
    public class LoanSignedTermSheetMessage
    {
        public Guid LoanId { get; set; }
        public string FileName { get; set; }
        public int ActivityId { get; set; }
        public string DeveloperId { get; set; }
    }
}
