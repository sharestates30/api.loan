﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bus.Messages
{
    public class LoanNotificationMessage
    {
        public Guid LoanId { get; set; }
        public string DeveloperId { get; set; }
        public string DeveloperName { get; set; }
        public string DeveloperEmail { get; set; }
        public string DeveloperPhone { get; set; }
        public string BrokerId { get; set; }
        public string BrokerName { get; set; }
        public string BrokerEmail { get; set; }
        public string BrokerPhone { get; set; }
        public bool IsBorrower { get; set; }
        public string Comment { get; set; }
    }
}
