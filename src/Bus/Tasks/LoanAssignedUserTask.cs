﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanAssignedUserTask : IJob
    {
        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;
        private readonly ISendEmailService _sendEmailService;

        public LoanAssignedUserTask(CloudDataService cloudDataService, ITokenService tokenService, ILoanResolverService loanResolver, ISendEmailService sendEmailService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
            this._sendEmailService = sendEmailService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanAssignUserQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<UserAssignedMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                // Get api url
                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
                var userApi = System.Configuration.ConfigurationManager.AppSettings["userApi"];

                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);

                var response = httpClient.GetAsync($"{userApi}/user/{message.UserId}").GetAwaiter().GetResult();
                var user = response.ReadAsObject<UserViewModel>();

                // notify
                var emails = new List<RecipientViewModel>();
                emails.Add(new RecipientViewModel { Email = user.Email, FirstName = user.FirstName, LastName = user.LastName });

                var model = new List<DataModelMessage>();

                this._sendEmailService.Send(emails, model, "AssignedUser").GetAwaiter().GetResult();
            }).GetAwaiter().GetResult();
        }
    }
}
