﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanPendingToAuthorizeTask : IJob
    {

        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;
        private readonly ILoanResolverService _loanResolver;
        private readonly ISendEmailService _sendEmailService;

        public LoanPendingToAuthorizeTask(CloudDataService cloudDataService, ITokenService tokenService, ILoanResolverService loanResolver, ISendEmailService sendEmailService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
            this._loanResolver = loanResolver;
            this._sendEmailService = sendEmailService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanPendingToAuthorizeQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                // Get api url
                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];

                var loan = this._loanResolver.GetLoan(message.LoanId, token).GetAwaiter().GetResult();

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);

                if ( loan.Developer != null && !string.IsNullOrEmpty(loan.Developer.Email))
                {
                    // notify to borrower and admin.
                    var emails = new List<RecipientViewModel>();

                    emails.Add(new RecipientViewModel { Email = loan.Developer.Email, FirstName = loan.Developer.FirstName, LastName = loan.Developer.LastName });

                    var model = new List<DataModelMessage>();
                    model.Add(new DataModelMessage { Key = "loanId", Value = loan.Loan.LoanId.ToString() });
                    model.Add(new DataModelMessage { Key = "userName", Value = $"{loan.Developer.FirstName} {loan.Developer.LastName}" });

                    this._sendEmailService.Send(emails, model, "LoanPendingToAuthorize").GetAwaiter().GetResult();
                }


            }).GetAwaiter().GetResult();

        }
    }
}
