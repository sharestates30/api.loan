﻿using Bus.Messages;
using Bus.Models;
using Bus.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Bus.Tasks
{
    public class LoanClosedTask : IJob
    {

        private readonly CloudDataService _cloudDataService;
        private readonly ITokenService _tokenService;

        public LoanClosedTask(CloudDataService cloudDataService, ITokenService tokenService)
        {
            this._cloudDataService = cloudDataService;
            this._tokenService = tokenService;
        }

        public void Execute(IJobExecutionContext context)
        {
            var queue = this._cloudDataService.GetQueue(QueueNames.LoanClosedQueue);
            var claim = this._cloudDataService.ReceiveMessage(queue).GetAwaiter().GetResult();

            this._cloudDataService.ProcessMessage<LoanMessage>(queue, claim, (message) =>
            {
                // Get token
                var token = this._tokenService.GetToken().GetAwaiter().GetResult();

                var loanApi = System.Configuration.ConfigurationManager.AppSettings["loanApi"];
                var projectApi = System.Configuration.ConfigurationManager.AppSettings["projectApi"];

                var httpClient = new HttpClient();
                httpClient.SetBearerToken(token.access_token);

                var loan = httpClient.GetAsync($"{loanApi}/loan/{message.LoanId}").GetAwaiter().GetResult().ReadAsObject<LoanViewModel>();

                // create the project.
                var model = new ProjectPostViewModel();

                model.ProjectName = $"Project {loan.Address}";
                model.Address = loan.Address;
                model.ZipCode = loan.ZipCode;
                model.State = loan.State;
                model.City = loan.City;
                model.LoanNumber = loan.LoanNumber;
                model.PurchaseOrRefinanceId = loan.TransactionType.ToString();
                model.DevelopmentPhaseId = "3";
                model.PendingProjectAssetTypeId = loan.AssetType.ToString();
                model.OccupancyId = loan.Occupancy.ToString();
                model.DesiredFundingAmount = loan.AsIsValue;
                model.DesiredFundingDate = loan.DesiredFundingDate;
                model.RequestedTerm = loan.LoanTermRequest;
                model.RequestedLoanAmount = loan.LoanRequest1;
                model.AfterConstructionValue = 0;
                model.AcquisitionPrice = loan.PurchasePrice;
                model.PendingProjectStatusId = "1";
                model.BrokerId = loan.BrokerId.ToString();
                model.BrokerName = loan.BrokerAgentName;
                model.BorrowerId = loan.BrokerId.ToString();
                model.BorrowerName = loan.BorrowerName;
                model.AppraiserId = loan.AppraiserId.ToString();
                model.AppraiserName = loan.AppraiserName;
                model.BuyerAttorneyId = loan.BuyerId.ToString();
                model.BuyerAttorneyName = loan.BuyerLawFirm;
                model.SellerAttorneyId = loan.SellerId.ToString();
                model.SellerAttorneyName = loan.SellerLawFirm;

                var response = httpClient.PostAsync($"{projectApi}/project/pipeline", new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")).GetAwaiter().GetResult();

                if (!response.IsSuccessStatusCode)
                    throw new Exception(response.Content.ReadAsStringAsync().Result);

            }).GetAwaiter().GetResult();

        }
    }
}
